<?php
//namespace App\Models\Catering;


// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\PackageSeeder;
use Illuminate\Database\Seeder;
use App\Models\Catering\Package;
use App\Models\Catering\Dish;
use App\Models\Catering\Addon;


class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      //$sections = config('package.sections');
      $main = 'main';
      $side_dishes = 'sidedish';
      $vegetable = 'vegetable';
      $fish = 'fish';
      $prawn = 'prawn';
      $fishprawn = 'fishprawn';
      $seafood = 'seafood';
      $chicken = 'chicken';
      $appetizer = 'appetizer';
      $finger_food = 'fingerfood';
      $dessert = 'dessert';
      $beverage = 'beverage';
      $signature = 'signaturedish';
      $soup = 'soup';
      $salad = 'salad';
      $sandwich = 'sandwich';
      $dimsum = 'dimsum';
      $pasteries = 'pasteries';
      $kueh = 'kueh';

      //no spaces allowed. they must be identical to form $_POST['name']
      $high_tea_1 = Package::create([
          'name' => 'High Tea Package 1',
          'price_pax' => 3.00,
          'min_pax' => 100,
          'type' => 'hightea',
          'description' => '
          $3 / Person.
          Includes 3 Dishes + 1 Drink.
          Minimum Charge Of 100 Persons.
          Free Hot Coffee and Tea',
          'limit' => 4,
          'rules' => array(
            'salad' => 1,
            'sandwich' => 1,
            'chicken' => 1,
            'dimsum' => 1,
            'kueh' => 1,
            'fingerfood' => 1,
            'pasteries' => 1,
            'dessert' => 1,
            'beverage' => 1,
          ),
        ]);

        $high_tea_2 = Package::create([
          'name' => 'High Tea Package 2',
          'price_pax' => 4.00,
          'min_pax' => 80,
          'type' => 'hightea',
          'description' => '
          $4 / Person.
          Includes 4 Dishes + 1 Drink.
          Minimum Charge Of 80 Persons.
          Free Hot Coffee and Tea',
          'limit' => 5,
          'rules' => array(
            'salad' => 1,
            'sandwich' => 1,
            'chicken' => 1,
            'dimsum' => 1,
            'kueh' => 1,
            'fingerfood' => 1,
            'pasteries' => 1,
            'dessert' => 1,
            'beverage' => 1,
          ),
        ]);

        $high_tea_3 = Package::create([
          'name' => 'High Tea Package 3',
          'price_pax' => 5.00,
          'min_pax' => 60,
          'type' => 'hightea',
          'description' => '
          $5 / Person.
          Includes 5 Dishes + 1 Drink.
          Minimum Charge Of 60 Persons.
          Free Hot Coffee and Tea',
          'limit' => 6,
          'rules' => array(
            'salad' => 1,
            'sandwich' => 1,
            'chicken' => 1,
            'dimsum' => 1,
            'kueh' => 1,
            'fingerfood' => 1,
            'pasteries' => 1,
            'dessert' => 1,
            'beverage' => 1,
          ),
        ]);

        $high_tea_4 = Package::create([
          'name' => 'High Tea Package 4',
          'price_pax' => 6.00,
          'min_pax' => 60,
          'type' => 'hightea',
          'description' => '
          $6 / Person.
          Includes 6 Dishes + 1 Drink.
          Minimum Charge Of 60 Persons.
          Free Hot Coffee and Tea',
          'limit' => 7,
          'rules' => array(
            'salad' => 1,
            'sandwich' => 1,
            'chicken' => 1,
            'dimsum' => 1,
            'kueh' => 1,
            'fingerfood' => 1,
            'pasteries' => 1,
            'dessert' => 1,
            'beverage' => 1,
          ),
        ]);

        $high_tea_5 = Package::create([
          'name' => 'High Tea Package 5',
          'price_pax' => 7.00,
          'min_pax' => 60,
          'type' => 'hightea',
          'description' => '
          $7 / Person.
          Includes 7 Dishes + 1 Drink.
          Minimum Charge Of 60 Persons.
          Free Hot Coffee and Tea',
          'limit' => 8,
          'rules' => array(
            'salad' => 1,
            'sandwich' => 1,
            'chicken' => 1,
            'dimsum' => 1,
            'kueh' => 1,
            'fingerfood' => 1,
            'pasteries' => 1,
            'dessert' => 1,
            'beverage' => 1,
          ),
        ]);

        $bento_standard = Package::create([
          'name' => 'Bento Standard',
          'price_pax' => 10.00,
          'min_pax' => 5,
          'type' => 'bento',
          'description' => '
          $10 / Person.
          5 Dishes + 1 Drink Of The Day.
          Minimum Charge Of 5 Persons',
          'limit' => 5,
          'rules' => array(
            'main' => 1,
            'sidedish' => 2,
            'vegetable' => 1,
            'soup' => 1,
            'beverage' =>1,
          )

        ]);

        $bento_value = Package::create([
          'name' => 'Bento Value',
          'price_pax' => 7.00,
          'min_pax' => 10,
          'type' => 'bento',
          'description' => '
          $7 / Person.
          1 Main Dish + 1 Drink Of The Day.
          Minimum Charge Of 10 Persons',
          'limit' => 1,
          'rules' => array(
            'main' => 1,
            'sidedish' => 0,
            'vegetable' => 0,
            'soup' => 0,
            'beverage' =>0,
          )


        ]);

        $thai_value = Package::create([
          'name' => 'Thai Value Package',
          'price_pax' => 12.00,
          'min_pax' => 30,
          'type' => 'thai',
          'description' => '
          $12 / Person.
          7 Dishes + 1 Drink.
          Minimum Charge Of 30 Persons',
          'limit' => 8,
          'rules' => array(
            'main' => 1,
            'vegetable' => 1,
            'fish' => 1,
            'chicken' => 1,
            'appetizer' => 1,
            'fingerfood' => 1,
            'dessert' => 1,
            'beverage' => 1,
          )

        ]);

        $thai_standard = Package::create([
          'name' => 'Thai Standard Package',
          'price_pax' => 16.00,
          'min_pax' => 25,
          'type' => 'thai',
          'description' => '
          $16 / Person.
          8 Dishes + 1 Drink.
          Minimum Charge Of 25 Persons',
          'limit' => 9,
          'rules' => array(
            'main' => 1,
            'vegetable' => 1,
            'fishprawn' => 1,
            'chicken' => 1,
            'appetizer' => 1,
            'fingerfood' => 1,
            'dessert' => 1,
            'beverage' => 1,
            'signature' => 1,
          )

        ]);

        $thai_gastronomy = Package::create([
          'name' => 'Thai Gastronomy Package',
          'price_pax' => 20.00,
          'min_pax' => 30,
          'type' => 'thai',
          'description' => '
          $20 / Person.
          8 Dishes + 1 Drink.
          Minimum Charge Of 30 Persons',
          'limit' => 9,
          'rules' => array(
            'main' => 1,
            'vegetable' => 1,
            'fishprawn' => 1,
            'chicken' => 1,
            'appetizer' => 1,
            'fingerfood' => 1,
            'dessert' => 1,
            'beverage' => 1,
            'signature' => 1,
          )

        ]);

        $asian_value = Package::create([
          'name' => 'Asian Value Package',
          'price_pax' => 12.00,
          'min_pax' => 30,
          'type' => 'asian',
          'description' => '$12 / Person. 7 Dishes + 1 Drink. Minimum Charge Of 30 Persons',
          'limit' => 7,
          'rules' => array(
            'main' => 1,
            'vegetable' => 1,
            'fish' => 1,
            'chicken' => 1,
            'appetizer' => 1,
            'dessert' => 1,
            'beverage' => 1,
          )

        ]);

        $asian_standard = Package::create([
          'name' => 'Asian Standard Package',
          'price_pax' => 16.00,
          'min_pax' => 25,
          'type' => 'asian',
          'description' => '$16 / Person. 8 Dishes + 1 Drink. Minimum Charge Of 25 Persons',
          'limit' => 9,
          'rules' => array(
            'main' => 1,
            'vegetable' => 1,
            'fish' => 1,
            'chicken' => 1,
            'prawn' => 1,
            'appetizer' => 1,
            'dessert' => 1,
            'beverage' => 1,
            'signature' => 1,
          )

        ]);

        $asian_gastronomy = Package::create([
          'name' => 'Asian Gastronomy Package',
          'price_pax' => 20.00,
          'min_pax' => 30,
          'type' => 'asian',
          'description' => '$20 / Person. 9 Dishes + 1 Drink. Minimum Charge Of 30 Persons',
          'limit' => 9,
          'rules' => array(
            'main' => 1,
            'vegetable' => 1,
            'fish' => 1,
            'chicken' => 1,
            'seafood' => 1,
            'fingerfood' => 1,
            'gastronomydessert' => 2,
            'beverage' => 1,
            'signature' => 1,
          )

        ]);

        $addon_side_one = Addon::create([
          'name' => 'Thai Roasted Duck Curry (1/2 Duck)',
          'price' => '$25.00 per serving',
          'price_pax' => 25.00,
          'min_pax' => '1/2 Duck Per Serving',
          'type' => 'addons',
          'description' => 'Add-On Side Dishes',
          'placeholder' => '1',
          'limit' => 0,
          'slug' =>'thai_roasted_duck_curry_half_duck',
        ]);

        $addon_side_two = Addon::create([
          'name' => 'Thai Roasted Duck Curry (Whole Duck)',
          'price' => '$35.00 per serving',
          'price_pax' => 35.00,
          'min_pax' => '1 Duck Per Serving',
          'type' => 'addons',
          'description' => 'Add-On Side Dishes',
          'placeholder' => '1',
          'limit' => 0,
          'slug' => 'thai_roasted_duck_curry_whole_duck',
        ]);

        $addon_side_three = Addon::create([
          'name' => 'Thai Green Curry',
          'price' => '$1.50 per person',
          'price_pax' => 1.50,
          'min_pax' => 'Minimum order of 20 persons',
          'type' => 'addons',
          'description' => 'Add-On Side Dishes',
          'placeholder' => '20',
          'limit' => 19,
          'slug' => 'thai_green_curry',
        ]);

        $addon_side_four = Addon::create([
          'name' => 'Thai Red Curry',
          'price' => '$1.50 per person',
          'price_pax' => 1.50,
          'min_pax' => 'Minimum order of 20 persons',
          'type' => 'addons',
          'description' => 'Add-On Side Dishes',
          'placeholder' => '20',
          'limit' => 19,
          'slug'  => 'thai_red_curry',
        ]);

        $addon_side_five = Addon::create([
          'name' => 'Tom Yum Fried Bee Hoon',
          'price' => '$1.00 per person',
          'price_pax' => 1.00,
          'min_pax' => 'Minimum order of 20 persons',
          'type' => 'addons',
          'description' => 'Add-On Side Dishes',
          'placeholder' => '20',
          'limit' => 19,
          'slug' => 'tom_yum_fried_bee_hoon',
        ]);

        $addon_side_six = Addon::create([
          'name' => 'Signature Phad Thai Noodles',
          'price' => '$1.00 per person',
          'price_pax' => 1.00,
          'min_pax' => 'Minimum order of 20 persons',
          'type' => 'addons',
          'description' => 'Add-On Side Dishes',
          'placeholder' => '20',
          'limit' => 19,
          'slug' => 'signature_phad_thai_noodles',
        ]);

        $thai_basil_chicken_main = Dish::create([
          'name' => 'Thai Basil Chicken',
          'section' => $main
        ]);

        $thai_red_curry_chicken_main = Dish::create([
          'name' => 'Thai Red Curry Chicken',
          'section' => $main
        ]);

        $thai_green_curry_chicken_main = Dish::create([
          'name' => 'Thai Green Curry Chicken',
          'section' => $main
        ]);

        $bbq_sotong_main = Dish::create([
          'name' => 'BBQ Sotong with Thai Chilli',
          'section' => $main
        ]);

        $thai_chilli_fish_main = Dish::create([
          'name' => 'Thai Chilli Fish',
          'section' => $main
        ]);

        $phad_thai_main = Dish::create([
          'name' => 'Phad Thai',
          'section' => $main
        ]);

        $seafood_pineapple_main = Dish::create([
          'name' => 'Seafood Pineapple Fried Rice',
          'section' => $main
        ]);

        $seafood_fried_rice_main = Dish::create([
          'name' => 'Seafood Fried Rice',
          'section' => $main
        ]);

        $pineapple_fried_rice_main = Dish::create([
          'name' => 'Pineapple Fried Rice',
          'section' => $main
        ]);

        $black_olive_rice_main = Dish::create([
          'name' => 'Black Olive Fried Rice',
          'section' => $main
        ]);

        $tomyum_fried_rice_main = Dish::create([
          'name' => 'Tom Yum Fried Rice',
          'section' => $main
        ]);

        $beef_fried_rice_main = Dish::create([
          'name' => 'Beef Fried Rice',
          'section' => $main
        ]);

        $basil_leaves_rice_main = Dish::create([
          'name' => 'Thai Basil Leaves (Chicken/Beef) Rice',
          'section' => $main
        ]);

        $yangchow_rice_main = Dish::create([
          'name' => 'Yang Chow Fried Rice',
          'section' => $main
        ]);

        $green_curry_rice_main = Dish::create([
          'name' => 'Thai Green Curry Fried Rice',
          'section' => $main
        ]);

        $hongkong_fried_noodle_main = Dish::create([
          'name' => 'Hong Kong Fried Noodle',
          'section' => $main
        ]);

        $tomyum_fried_beehoon_main = Dish::create([
          'name' => 'Tom Yum Fried Bee Hoon',
          'section' => $main
        ]);

        $sambal_fried_rice_main = Dish::create([
          'name' => 'Sambal Fried Rice',
          'section' => $main
        ]);

        $hk_style_baked_rice_main = Dish::create([
          'name' => 'Hong Kong Style Baked Rice',
          'section' => $main
        ]);

        $olive_rice_anchovies_main = Dish::create([
          'name' => 'Olive Fried Rice with Golden Anchovies',
          'section' => $main
        ]);

        $tomyum_seafood_bee_hoon_main = Dish::create([
          'name' => 'Tom Yum Seafood Fried Bee Hoon',
          'section' => $main
        ]);

        $sin_chow_bee_hoon_main = Dish::create([
          'name' => 'Sin Chow Bee Hoon',
          'section' => $main
        ]);

        $hk_claypot_mushroom_chicken = Dish::create([
          'name' => 'HK Claypot Mushrooms and Chicken Rice',
          'section' => $main
        ]);

        $nasi_briyani_main = Dish::create([
          'name' => 'Nasi Briyani Chicken Rice',
          'section' => $main
        ]);

        $mee_goreng_main = Dish::create([
          'name' => 'Mee Goreng',
          'section' => $main
        ]);

        $eefu_noodles_main = Dish::create([
          'name' => 'Braised Ee Fu Noodles with Dried Scallop',
          'section' => $main
        ]);

        $sweet_sour_fishprawn = Dish::create([
          'name' => 'Sweet and Sour Fish',
          'section' => $fishprawn
        ]);

        $cereal_fish_fishprawn = Dish::create([
          'name' => 'Cereal Fish',
          'section' => $fishprawn
        ]);

        $cereal_prawn_fishprawn = Dish::create([
          'name' => 'Cereal Prawn',
          'section' => $fishprawn
        ]);

        $thai_fish_fishprawn = Dish::create([
          'name' => 'Thai Chilli Fish',
          'section' => $fishprawn
        ]);

        $soya_fish_fishprawn = Dish::create([
          'name' => 'Light Soya Sauce Fish',
          'section' => $fishprawn
        ]);

        $mixed_seafood_fishprawn =Dish::create([
          'name' => 'Mixed Seafood with Basil Leaves',
          'section' => $fishprawn
        ]);

        $sweet_sour_prawn = Dish::create([
          'name' => 'Sweet and Sour Prawn',
          'section' => $fishprawn
        ]);

        $yellow_curry_fishprawn = Dish::create([
          'name' => 'Prawns with Yellow Curry Powder',
          'section' => $fishprawn
        ]);

        $cereal_prawn_prawn = Dish::create([
          'name' => 'Cereal Prawn',
          'section' => $prawn
        ]);

        $tempura_prawn = Dish::create([
          'name' => 'Tempura Prawn',
          'section' => $prawn
        ]);

        $thai_style_prawn = Dish::create([
          'name' => 'Thai Style Prawn',
          'section' => $prawn
        ]);

        $butter_prawn = Dish::create([
          'name' => 'Butter Prawn with Fried Garlic',
          'section' => $prawn
        ]);

        $wasabi_prawn = Dish::create([
          'name' => 'Wasabi Prawn',
          'section' => $prawn
        ]);

        $thai_style_bbq_prawn = Dish::create([
          'name' => 'Thai Style BBQ Prawn',
          'section' => $prawn
        ]);

        $battered_prawn_chilli_crab = Dish::create([
          'name' => 'Battered Prawn with Chilli Crab Sauce',
          'section' => $seafood
        ]);

        $herbal_prawn_wolfberries = Dish::create([
          'name' => 'Herbal Prawn with Wolfberries',
          'section' => $seafood
        ]);

        $bbq_sotong_green_chilli = Dish::create([
          'name' => 'BBQ Sotong with Thai Green Chilli Dressing',
          'section' => $seafood
        ]);

        $bbq_sotong_thai_style = Dish::create([
          'name' => 'Thai Style BBQ Sotong',
          'section' => $prawn
        ]);

        $scallop_egg_white = Dish::create([
          'name' => 'Braised Scallop with Egg White Sauce',
          'section' => $seafood
        ]);

        $sambal_tunis_prawn = Dish::create([
          'name' => 'Sambal Tunis Prawn',
          'section' => $seafood
        ]);

        $sunny_side = Dish::create([
          'name' => 'Sunny Side Up Eggs',
          'section' => $side_dishes
        ]);


        $sotong_youtiao_side = Dish::create([
          'name' => 'Sotong You Tiao',
          'section' => $side_dishes
        ]);


        $ngoh_hiang = Dish::create([
          'name' => 'Ngoh Hiang',
          'section' => $side_dishes
        ]);

        $sotong_ball_side = Dish::create([
          'name' => 'Sotong Ball',
          'section' => $side_dishes
        ]);

        $chicken_luncheon_side = Dish::create([
          'name' => 'Chicken Luncheon Meat',
          'section' => $side_dishes
        ]);

        $tomyum_soup = Dish::create([
          'name' => 'Tom Yum Soup Clear',
          'section' => $soup
        ]);

        $mushroom_soup = Dish::create([
          'name' => 'Mushroom with Egg Fower Soup',
          'section' => $soup
        ]);

        $mango_salad_salad = Dish::create([
          'name' => 'Thai Mango Salad',
          'section' => $salad
        ]);

        $potato_salad_salad = Dish::create([
          'name' => 'Potato Salad',
          'section' => $salad
        ]);

        $mesculun_salad_salad = Dish::create([
          'name' => 'Mesculun Salad',
          'section' => $salad
        ]);

        $caesar_salad_salad = Dish::create([
          'name' => 'Caesar Salad',
          'section' => $salad
        ]);

        $egg_mayo_sandwich = Dish::create([
          'name' => 'Egg Mayo Sandwich',
          'section' => $sandwich
        ]);

        $tuna_mayo_sandwich = Dish::create([
          'name' => 'Tuna Mayo Sandwich',
          'section' => $sandwich
        ]);

        $vegan_sandwich = Dish::create([
          'name' => 'Vegan Sandwich',
          'section' => $sandwich
        ]);

        $chicken_ham_sandwich = Dish::create([
          'name' => 'Chicken Ham Sandwich',
          'section' => $sandwich
        ]);

        $curry_potato_sandwich = Dish::create([
          'name' => 'Curry Potato Sandwich',
          'section' => $sandwich
        ]);

        $siew_mai_dimsum = Dish::create([
          'name' => 'Steamed Siew Mai',
          'section' => $dimsum
        ]);

        $har_kau_dimsum = Dish::create([
          'name' => 'Steamed Har Kau',
          'section' => $dimsum
        ]);

        $yam_cake_dimsum = Dish::create([
          'name' => 'Mini Yam Cake',
          'section' => $dimsum
        ]);

        $char_siew_dimsum = Dish::create([
          'name' => 'Mini Char Siew Chicken Pau',
          'section' => $dimsum
        ]);

        $red_bean_pau_dimsum = Dish::create([
          'name' => 'Mini Red Bean Pau',
          'section' => $dimsum
        ]);

        $nonya_kueh = Dish::create([
          'name' => 'Assorted Nonya Kueh',
          'section' => $kueh
        ]);

        $rice_kueh = Dish::create([
          'name' => 'Mini Rice Kueh',
          'section' => $kueh
        ]);

        $soon_kueh = Dish::create([
          'name' => 'Mini Soon Kueh',
          'section' => $kueh
        ]);

        $mini_profiteroles = Dish::create([
          'name' => 'Mini Profiteroles',
          'section' => $pasteries
        ]);

        $mini_choco_eclair = Dish::create([
          'name' => 'Mini Chocolate Eclair',
          'section' => $pasteries
        ]);

        $mini_swiss_roll = Dish::create([
          'name' => 'Mini Swiss Roll',
          'section' => $pasteries
        ]);

        $mini_egg_tart = Dish::create([
          'name' => 'Mini Egg Tart',
          'section' => $pasteries
        ]);

        $mini_fruit_tart = Dish::create([
          'name' => 'Mini Fruit Tart',
          'section' => $pasteries
        ]);

        $mini_fruit_tart_pasteries = Dish::create([
          'name' => 'Mini Fruit Tart',
          'section' => $pasteries
        ]);

        $lime_juice = Dish::create([
          'name' => 'Lime Juice',
          'section' => $beverage
        ]);

        $barley = Dish::create([
          'name' => 'Barley',
          'section' => $beverage
        ]);

        $fruit_punch = Dish::create([
          'name' => 'Fruit Punch',
          'section' => $beverage
        ]);

        $thai_ice_tea = Dish::create([
          'name' => 'Thai Iced Tea',
          'section' => $beverage
        ]);

        $thai_ice_tea_asian_value = Dish::create([
          'name' => 'Thai Iced Tea (+ $0.50 / person)',
          'section' => $beverage
        ]);

        $thai_ice_tea_thai_value = Dish::create([
          'name' => 'Thai Iced Tea (+ $0.50 / person)',
          'section' => $beverage
        ]);

        $milk_tea_beverage = Dish::create([
          'name' => 'Thai Milk Tea',
          'section' => $beverage
        ]);

        $bandung = Dish::create([
          'name' => 'Bandung (+ $0.30 / person)',
          'section' => $beverage
        ]);

        $bandung_thai_gastronomy = Dish::create([
          'name' => 'Bandung',
          'section' => $beverage
        ]);

        $bandung_asian_gastronomy = Dish::create([
          'name' => 'Bandung',
          'section' => $beverage
        ]);

        $bandung_thai_standard = Dish::create([
          'name' => 'Bandung',
          'section' => $beverage
        ]);

        $bandung_asian_standard = Dish::create([
          'name' => 'Bandung',
          'section' => $beverage
        ]);

        $bandung_high_tea = Dish::create([
          'name' => 'Bandung',
          'section' => $beverage
        ]);

        $bird_nest_drink = Dish::create([
          'name' => 'Thai Bird Nest Drink',
          'section' => $beverage
        ]);

        $thai_basil_chicken_signature = Dish::create([
          'name' => 'Thai Basil Chicken',
          'section' => $signature
        ]);

        $thai_beef_stew_signature = Dish::create([
          'name' => 'Thai Style Beef Stew',
          'section' => $signature
        ]);

        $thai_duck_curry_signature = Dish::create([
          'name' => 'Thai Roasted Duck with Curry',
          'section' => $signature
        ]);

        $tom_yum_scallop_signature = Dish::create([
          'name' => 'Tom Yum Seafood Soup with Scallop',
          'section' => $signature
        ]);

        $prawn_cake_signature = Dish::create([
          'name' => 'Home Made Prawn Cake',
          'section' => $signature
        ]);

        $tom_yum_fish_signature = Dish::create([
          'name' => 'Tom Yum Fried Fish',
          'section' => $signature
        ]);

        $tom_yum_signature = Dish::create([
          'name' => 'Tom Yum Seafood Soup',
          'section' => $signature
        ]);

        $fish_cake_signature = Dish::create([
          'name' => 'Home Made Fish Cake',
          'section' => $signature
        ]);

        $green_curry_chicken_signature = Dish::create([
          'name' => 'Green Curry Chicken',
          'section' => $signature
        ]);

        $red_curry_chicken_signature = Dish::create([
          'name' => 'Red Curry Chicken',
          'section' => $signature
        ]);

        $chicken_wing_chilli_signature = Dish::create([
          'name' => 'Chicken Wing w Thai Chilli Sauce',
          'section' => $signature
        ]);

        $maggie_mee_signature = Dish::create([
          'name' => 'Maggie Mee Seafood Salad',
          'section' => $signature
        ]);

        $chocolate_eclair_dessert = Dish::create([
          'name' => 'Chocolate Eclair',
          'section' => $dessert
        ]);

        $vanilla_profiteroles = Dish::create([
          'name' => 'Mini Vanilla Profiteroles',
          'section' => $dessert
        ]);

        $red_ruby = Dish::create([
          'name' => 'Red Ruby with Coconut Cream (+ $0.50 / person)',
          'section' => $dessert
        ]);

        $red_ruby_thai_gastronomy = Dish::create([
          'name' => 'Red Ruby with Coconut Cream',
          'section' => $dessert
        ]);

        $mango_sticky_rice = Dish::create([
          'name' => 'Mango Sticky Rice (+ $1.00 / person)',
          'section' => $dessert
        ]);

        $mango_sticky_rice_thai_gastronomy = Dish::create([
          'name' => 'Mango Sticky Rice',
          'section' => $dessert
        ]);

        $cheesecake_dessert = Dish::create([
          'name' => 'New York Cheesecake',
          'section' => $dessert
        ]);

        $chin_chow = Dish::create([
          'name' => 'Chin Chow with Logan',
          'section' => $dessert
        ]);

        $almond_jelly_dessert = Dish::create([
          'name' => 'Almond Jelly with Logan',
          'section' => $dessert
        ]);

        $cheng_tng = Dish::create([
          'name' => 'Cheng Tng',
          'section' => $dessert
        ]);

        $tapioca = Dish::create([
          'name' => 'Tapioca',
          'section' => $dessert
        ]);

        $mixed_fruit_cocktail = Dish::create([
          'name' => 'Mixed Fruit Cocktail',
          'section' => $dessert
        ]);

        $fresh_fruit_dessert = Dish::create([
          'name' => 'Fresh Fruits Platter',
          'section' => $dessert
        ]);

        $sea_coconut_dessert = Dish::create([
          'name' => 'Sea Coconut with Cocktail',
          'section' => $dessert
        ]);

        $fish_ball = Dish::create([
          'name' => 'Fish Ball',
          'section' => $finger_food
        ]);

        $sotong_youtiao = Dish::create([
          'name' => 'Sotong You Tiao',
          'section' => $finger_food
        ]);

        $curry_puff = Dish::create([
          'name' => 'Curry Puff',
          'section' => $finger_food
        ]);

        $curry_samosa = Dish::create([
          'name' => 'Curry Samosa',
          'section' => $finger_food
        ]);

        $spring_roll = Dish::create([
            'name' => 'Spring Roll',
          'section' => $finger_food
        ]);

        $prawn_roll = Dish::create([
            'name' => 'Prawn Roll',
          'section' => $finger_food
        ]);

        $breaded_scallop = Dish::create([
          'name' => 'Breaded Scallop',
          'section' => $finger_food
        ]);

        $cuttle_fish_ball = Dish::create([
          'name' => 'Cuttle Fish Ball',
          'section' => $finger_food
        ]);

        $pandan_leaf_chicken = Dish::create([
          'name' => 'Pandan Leaf Chicken',
          'section' => $finger_food
        ]);

        $deep_fried_chicken = Dish::create([
          'name' => 'Deep Fried Chicken',
          'section' => $finger_food
        ]);

        $yam_roll_scallop = Dish::create([
          'name' => 'Yam Roll with Scallop',
          'section' => $finger_food
        ]);

        $crispy_honey_chicken = Dish::create([
          'name' => 'Crispy Honey Chicken Wing',
          'section' => $finger_food
        ]);

        $chicken_ngoh_hiang_app = Dish::create([
          'name' => 'Chicken Ngoh Hiang',
          'section' => $appetizer
        ]);

        $ngoh_hiang_app = Dish::create([
          'name' => 'Ngoh Hiang',
          'section' => $appetizer
        ]);

        $mango_salad_app = Dish::create([
          'name' => 'Thai Mango Salad',
          'section' => $appetizer
        ]);

        $papaya_salad_app = Dish::create([
          'name' => 'Papaya Salad',
          'section' => $appetizer
        ]);

        $fish_ball_app = Dish::create([
          'name' => 'Fish Ball',
          'section' => $appetizer
        ]);

        $sotong_youtiao_app = Dish::create([
          'name' => 'Sotong You Tiao',
          'section' => $appetizer
        ]);

        $curry_samosa_app = Dish::create([
          'name' => 'Curry Samosa',
          'section' => $appetizer
        ]);

        $breaded_scallop_app = Dish::create([
          'name' => 'Breaded Scallop',
          'section' => $appetizer
        ]);

        $tom_yum_seafood_app = Dish::create([
          'name' => 'Tom Yum Seafood Soup',
          'section' => $appetizer
        ]);

        $tom_yum_seafood_app_extra = Dish::create([
          'name' => 'Tom Yum Seafood Soup (+ $0.50 / person)',
          'section' => $appetizer
        ]);

        $chicken_feet_salad_app = Dish::create([
          'name' => 'Chicken Feet Salad',
          'section' => $appetizer
        ]);

        $prawn_cake_app = Dish::create([
          'name' => 'Home Made Prawn Cake',
          'section' => $appetizer
        ]);

        $fish_cake_app = Dish::create([
          'name' => 'Home Made Fish Cake',
          'section' => $appetizer
        ]);

        $spring_roll_app = Dish::create([
          'name' => 'Thai Style Spring Roll',
          'section' => $appetizer
        ]);

        $battered_prawn_app = Dish::create([
          'name' => 'Battered Prawn with Chilli Crab Sauce',
          'section' => $appetizer
        ]);

        $herbal_prawn_app = Dish::create([
          'name' => 'Herbal Prawn with Wolfberries',
          'section' => $appetizer
        ]);

        $bbq_sotong_app = Dish::create([
          'name' => 'BBQ Sotong with Green Chilli Dressing',
          'section' => $appetizer
        ]);

        $braised_scallop_app = Dish::create([
          'name' => 'Braised Scallop with Egg White Sauce',
          'section' => $appetizer
        ]);

        $thai_basil_chicken_chicken = Dish::create([
          'name' => 'Thai Basil Chicken',
          'section' => $chicken
        ]);

        $thai_red_curry_chicken_chicken = Dish::create([
          'name' => 'Thai Red Curry Chicken',
          'section' => $chicken
        ]);

        $red_curry_chicken_drumstick = Dish::create([
          'name' => 'Thai Red Curry Chicken (Drumstick)',
          'section' => $chicken
        ]);

        $green_curry_chicken_drumstick = Dish::create([
          'name' => 'Thai Green Curry Chicken (Drumstick)',
          'section' => $chicken
        ]);

        $thai_green_curry_chicken_chicken = Dish::create([
          'name' => 'Thai Green Curry Chicken',
          'section' => $chicken
        ]);

        $bbq_midwing_chicken = Dish::create([
          'name' => 'BBQ Chicken Mid Wing',
          'section' => $chicken
        ]);


        $chicken_nuggets_chicken = Dish::create([
          'name' => 'Chicken Nuggets',
          'section' => $chicken
        ]);


        $seaweed_chicken_chicken = Dish::create([
          'name' => 'Seaweed Chicken Roll',
          'section' => $chicken
        ]);


        $teriyaki_midwing_chicken = Dish::create([
          'name' => 'Teriyaki Mid Wing',
          'section' => $chicken
        ]);

        $crispy_drumlets_chicken = Dish::create([
          'name' => 'Crisply Drumlets',
          'section' => $chicken
        ]);

        $honey_chicken = Dish::create([
          'name' => 'Honey Chicken',
          'section' => $chicken
        ]);

        $thai_lemon_chicken_cutlet = Dish::create([
          'name' => 'Thai Style Lemon Chicken Cutlet',
          'section' => $chicken
        ]);

        $deep_fried_chicken_basil = Dish::create([
          'name' => 'Deep Fried Chicken with Garlic Basil',
          'section' => $chicken
        ]);

        $chicken_cashew_nuts = Dish::create([
          'name' => 'Chicken with Cashew Nuts',
          'section' => $chicken
        ]);

        $blackpepper_chicken = Dish::create([
          'name' => 'Black Pepper Chicken',
          'section' => $chicken
        ]);

        $chicken_wing_thai_chilli = Dish::create([
          'name' => 'Chicken Wing with Thai Chilli Sauce',
          'section' => $chicken
        ]);

        $thai_curry_chicken = Dish::create([
          'name' => 'Thai Curry Chicken',
          'section' => $chicken
        ]);

        $crispy_midwing_chicken = Dish::create([
          'name' => 'Crispy Midwing Chicken',
          'section' => $chicken
        ]);

        $sweet_sour_chicken = Dish::create([
          'name' => 'Sweet and Sour Chicken',
          'section' => $chicken
        ]);

        $kung_pao_chicken = Dish::create([
          'name' => 'Kung Pao Chicken',
          'section' => $chicken
        ]);

        $thai_mango_chicken = Dish::create([
          'name' => 'Thai Mango Chicken',
          'section' => $chicken
        ]);

        $hk_roast_chicken = Dish::create([
          'name' => 'Hong Kong Style Roast Chicken',
          'section' => $chicken
        ]);

        $teriyaki_chicken_fillet = Dish::create([
          'name' => 'Teriyaki Chicken Fillet',
          'section' => $chicken
        ]);

        $thai_mango_chicken_fillet = Dish::create([
          'name' => 'Thai Style Mango Chicken Fillet',
          'section' => $chicken
        ]);

        $sweet_sour_fish = Dish::create([
          'name' => 'Sweet and Sour Fish',
          'section' => $fish
        ]);

        $cereal_fish = Dish::create([
          'name' => 'Cereal Fish',
          'section' => $fish
        ]);

        $thai_chilli_fish = Dish::create([
          'name' => 'Thai Chilli Sauce Fish',
          'section' => $fish
        ]);

        $light_soya_fish = Dish::create([
          'name' => 'Light Soya Sauce Fish',
          'section' => $fish
        ]);

        $cereal_prawn_fish = Dish::create([
          'name' => 'Cereal Prawn',
          'section' => $fish
        ]);

        $mixed_seafood_fish = Dish::create([
          'name' => 'Mixed Seafood with Basil Leaves',
          'section' => $fish
        ]);

        $sweet_sour_prawn_fish = Dish::create([
          'name' => 'Sweet and Sour Prawn',
          'section' => $fish
        ]);

        $prawns_yellow_curry_fish = Dish::create([
          'name' => 'Prawns with Yellow Curry Powder',
          'section' => $fish
        ]);

        $breaded_fish_mayo_fish = Dish::create([
          'name' => 'Breaded Fish with Mayo',
          'section' => $fish
        ]);

        $sambal_fish_fish = Dish::create([
          'name' => 'Sambal Fish',
          'section' => $fish
        ]);

        $tau_see_fish_fish = Dish::create([
          'name' => 'Tau See Fish',
          'section' => $fish
        ]);

        $spring_onion_fish_fish = Dish::create([
          'name' => 'Spring Onion Fish',
          'section' => $fish
        ]);

        $thai_chilli_fish_fish = Dish::create([
          'name' => 'Thai Chilli Fish',
          'section' => $fish
        ]);

        $light_soya_celery_fish_fish = Dish::create([
          'name' => 'Light Soya Celery Fish',
          'section' => $fish
        ]);

        $batter_wasabi_fish_fish = Dish::create([
          'name' => 'Battered Fish with Wasabi Mayo',
          'section' => $fish
        ]);

        $hk_style_fish = Dish::create([
          'name' => 'Hong Kong Style Steamed Fish',
          'section' => $fish
        ]);

        $thai_chilli_fried_fish = Dish::create([
          'name' => 'Thai Chilli Fried Fish',
          'section' => $fish
        ]);

        $mango_salsa_fish = Dish::create([
          'name' => 'Thai Mango Salsa Fish',
          'section' => $fish
        ]);

        $teochew_broth_fish = Dish::create([
          'name' => 'Teochew Broth Fish',
          'section' => $fish
        ]);

        $mix_vege_vegetable = Dish::create([
          'name' => 'Mixed Vegetables with Cabbage',
          'section' => $vegetable
        ]);

        $bean_sprout_thai_vege = Dish::create([
          'name' => 'Thai Style Bean Sprouts',
          'section' => $vegetable
        ]);

        $kangkong_vegetable = Dish::create([
          'name' => 'Stir Fried Kang Kong',
          'section' => $vegetable
        ]);

        $luohan_vegetable = Dish::create([
          'name' => 'Luo Han Vegetables',
          'section' => $vegetable
        ]);

        $baby_milk_vegetable = Dish::create([
          'name' => 'Baby Milk Vegetables',
          'section' => $vegetable
        ]);

        $stir_fried_vegetable = Dish::create([
          'name' => 'Stir Fried Mixed Vegetables',
          'section' => $vegetable
        ]);

        $stir_fried_beansprout = Dish::create([
          'name' => 'Stir Fried Bean Sprout',
          'section' => $vegetable
        ]);

        $kailan_mushrooms_vegetable = Dish::create([
          'name' => 'Kailan with Mushrooms',
          'section' => $vegetable
        ]);

        $sambal_longbean_vegetable = Dish::create([
          'name' => 'Sambal Long Bean',
          'section' => $vegetable
        ]);

        $stir_fried_french_bean = Dish::create([
          'name' => 'Stir Fried French Bean',
          'section' => $vegetable
        ]);

        $broccoli_mushrooms = Dish::create([
          'name' => 'Broccoli with Mushrooms',
          'section' => $vegetable
        ]);

        $stir_fried_broccoli_mushrooms = Dish::create([
          'name' => 'Stir Fried Mushrooms with Broccoli',
          'section' => $vegetable
        ]);

        $hongkong_kailan = Dish::create([
          'name' => 'Hong Kong Kailan',
          'section' => $vegetable
        ]);

        $baby_white_vegetable = Dish::create([
          'name' => 'Baby White Vegetables',
          'section' => $vegetable
        ]);

        $oyster_kailan_mushrooms = Dish::create([
          'name' => 'Oyster Kailan with Mushrooms',
          'section' => $vegetable
        ]);

        $broccoli_cauliflower = Dish::create([
          'name' => 'Broccoli and Cauliflower',
          'section' => $vegetable
        ]);

        $french_beans_xo_sauce = Dish::create([
          'name' => 'French Beans with XO Sauce',
          'section' => $vegetable
        ]);

        $luo_han_pacific_clams = Dish::create([
          'name' => 'Luo Han Vegetables with Pacific Clams',
          'section' => $vegetable
        ]);

        $luo_han_pacific_clams = Dish::create([
          'name' => 'Luo Han Vegetables with Pacific Clams',
          'section' => $vegetable
        ]);

        $broccoli_abalone_mushrooms = Dish::create([
          'name' => 'Broccoli with Abalone Mushrooms',
          'section' => $vegetable
        ]);

        $asparagus_tofu_vegetable = Dish::create([
          'name' => 'Thai Aspargus with Braised Tofu',
          'section' => $vegetable
        ]);

        $mushrooms_beancurd = Dish::create([
          'name' => 'Braised Mushrooms with Beancurd',
          'section' => $vegetable
        ]);

        $sayur_lodeh_tau_kwa = Dish::create([
          'name' => 'Sayur Lodeh with Tau Kwa',
          'section' => $vegetable
        ]);

        //high tea package choose 3
        $high_tea_1->dishes()->sync([
          $mango_salad_salad->id,
          $potato_salad_salad->id,
          $mesculun_salad_salad->id,
          $caesar_salad_salad->id,
          $egg_mayo_sandwich->id,
          $tuna_mayo_sandwich->id,
          $vegan_sandwich->id,
          $chicken_ham_sandwich->id,
          $curry_potato_sandwich->id,
          $siew_mai_dimsum->id,
          $har_kau_dimsum->id,
          $yam_cake_dimsum->id,
          $char_siew_dimsum->id,
          $red_bean_pau_dimsum->id,
          $nonya_kueh->id,
          $bbq_midwing_chicken->id,
          $chicken_nuggets_chicken->id,
          $seaweed_chicken_chicken->id,
          $teriyaki_midwing_chicken->id,
          $crispy_drumlets_chicken->id,
          $nonya_kueh->id,
          $rice_kueh->id,
          $soon_kueh->id,
          $fish_ball->id,
          $sotong_youtiao->id,
          $curry_puff->id,
          $curry_samosa->id,
          $spring_roll->id,
          $breaded_scallop->id,
          $mini_profiteroles->id,
          $mini_choco_eclair->id,
          $mini_swiss_roll->id,
          $mini_egg_tart->id,
          $mini_fruit_tart->id,
          $almond_jelly_dessert->id,
          $chin_chow->id,
          $fresh_fruit_dessert->id,
          $sea_coconut_dessert->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $thai_ice_tea->id,
          $bandung_high_tea->id
        ]);

        //high tea package choose 4
        $high_tea_2->dishes()->sync([
          $mango_salad_salad->id,
          $potato_salad_salad->id,
          $mesculun_salad_salad->id,
          $caesar_salad_salad->id,
          $egg_mayo_sandwich->id,
          $tuna_mayo_sandwich->id,
          $vegan_sandwich->id,
          $chicken_ham_sandwich->id,
          $curry_potato_sandwich->id,
          $siew_mai_dimsum->id,
          $har_kau_dimsum->id,
          $yam_cake_dimsum->id,
          $char_siew_dimsum->id,
          $red_bean_pau_dimsum->id,
          $nonya_kueh->id,
          $bbq_midwing_chicken->id,
          $chicken_nuggets_chicken->id,
          $seaweed_chicken_chicken->id,
          $teriyaki_midwing_chicken->id,
          $crispy_drumlets_chicken->id,
          $nonya_kueh->id,
          $rice_kueh->id,
          $soon_kueh->id,
          $fish_ball->id,
          $sotong_youtiao->id,
          $curry_puff->id,
          $curry_samosa->id,
          $spring_roll->id,
          $breaded_scallop->id,
          $mini_profiteroles->id,
          $mini_choco_eclair->id,
          $mini_swiss_roll->id,
          $mini_egg_tart->id,
          $mini_fruit_tart->id,
          $almond_jelly_dessert->id,
          $chin_chow->id,
          $fresh_fruit_dessert->id,
          $sea_coconut_dessert->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $thai_ice_tea->id,
          $bandung_high_tea->id

        ]);

        //high tea package choose 5
        $high_tea_3->dishes()->sync([
          $mango_salad_salad->id,
          $potato_salad_salad->id,
          $mesculun_salad_salad->id,
          $caesar_salad_salad->id,
          $egg_mayo_sandwich->id,
          $tuna_mayo_sandwich->id,
          $vegan_sandwich->id,
          $chicken_ham_sandwich->id,
          $curry_potato_sandwich->id,
          $siew_mai_dimsum->id,
          $har_kau_dimsum->id,
          $yam_cake_dimsum->id,
          $char_siew_dimsum->id,
          $red_bean_pau_dimsum->id,
          $nonya_kueh->id,
          $bbq_midwing_chicken->id,
          $chicken_nuggets_chicken->id,
          $seaweed_chicken_chicken->id,
          $teriyaki_midwing_chicken->id,
          $crispy_drumlets_chicken->id,
          $nonya_kueh->id,
          $rice_kueh->id,
          $soon_kueh->id,
          $fish_ball->id,
          $sotong_youtiao->id,
          $curry_puff->id,
          $curry_samosa->id,
          $spring_roll->id,
          $breaded_scallop->id,
          $mini_profiteroles->id,
          $mini_choco_eclair->id,
          $mini_swiss_roll->id,
          $mini_egg_tart->id,
          $mini_fruit_tart->id,
          $almond_jelly_dessert->id,
          $chin_chow->id,
          $fresh_fruit_dessert->id,
          $sea_coconut_dessert->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $thai_ice_tea->id,
          $bandung_high_tea->id

        ]);

        //high tea package choose 6
        $high_tea_4->dishes()->sync([
          $mango_salad_salad->id,
          $potato_salad_salad->id,
          $mesculun_salad_salad->id,
          $caesar_salad_salad->id,
          $egg_mayo_sandwich->id,
          $tuna_mayo_sandwich->id,
          $vegan_sandwich->id,
          $chicken_ham_sandwich->id,
          $curry_potato_sandwich->id,
          $siew_mai_dimsum->id,
          $har_kau_dimsum->id,
          $yam_cake_dimsum->id,
          $char_siew_dimsum->id,
          $red_bean_pau_dimsum->id,
          $nonya_kueh->id,
          $bbq_midwing_chicken->id,
          $chicken_nuggets_chicken->id,
          $seaweed_chicken_chicken->id,
          $teriyaki_midwing_chicken->id,
          $crispy_drumlets_chicken->id,
          $nonya_kueh->id,
          $rice_kueh->id,
          $soon_kueh->id,
          $fish_ball->id,
          $sotong_youtiao->id,
          $curry_puff->id,
          $curry_samosa->id,
          $spring_roll->id,
          $breaded_scallop->id,
          $mini_profiteroles->id,
          $mini_choco_eclair->id,
          $mini_swiss_roll->id,
          $mini_egg_tart->id,
          $mini_fruit_tart->id,
          $almond_jelly_dessert->id,
          $chin_chow->id,
          $fresh_fruit_dessert->id,
          $sea_coconut_dessert->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $thai_ice_tea->id,
          $bandung_high_tea->id
        ]);

        //high tea package choose 7
        $high_tea_5->dishes()->sync([
          $mango_salad_salad->id,
          $potato_salad_salad->id,
          $mesculun_salad_salad->id,
          $caesar_salad_salad->id,
          $egg_mayo_sandwich->id,
          $tuna_mayo_sandwich->id,
          $vegan_sandwich->id,
          $chicken_ham_sandwich->id,
          $curry_potato_sandwich->id,
          $siew_mai_dimsum->id,
          $har_kau_dimsum->id,
          $yam_cake_dimsum->id,
          $char_siew_dimsum->id,
          $red_bean_pau_dimsum->id,
          $nonya_kueh->id,
          $bbq_midwing_chicken->id,
          $chicken_nuggets_chicken->id,
          $seaweed_chicken_chicken->id,
          $teriyaki_midwing_chicken->id,
          $crispy_drumlets_chicken->id,
          $nonya_kueh->id,
          $rice_kueh->id,
          $soon_kueh->id,
          $fish_ball->id,
          $sotong_youtiao->id,
          $curry_puff->id,
          $curry_samosa->id,
          $spring_roll->id,
          $breaded_scallop->id,
          $mini_profiteroles->id,
          $mini_choco_eclair->id,
          $mini_swiss_roll->id,
          $mini_egg_tart->id,
          $mini_fruit_tart->id,
          $almond_jelly_dessert->id,
          $chin_chow->id,
          $fresh_fruit_dessert->id,
          $sea_coconut_dessert->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $thai_ice_tea->id,
          $bandung_high_tea->id

        ]);

        $bento_standard->dishes()->sync([
          $thai_basil_chicken_main->id,
          $thai_red_curry_chicken_main->id,
          $thai_green_curry_chicken_main->id,
          $bbq_sotong_main->id,
          $thai_chilli_fish_main->id,
          $sunny_side->id,
          $ngoh_hiang->id,
          $sotong_youtiao_side->id,
          $sotong_ball_side->id,
          $chicken_luncheon_side->id,
          $mix_vege_vegetable->id,
          $bean_sprout_thai_vege->id,
          $kangkong_vegetable->id,
          $luohan_vegetable->id,
          $baby_milk_vegetable->id,
          $tomyum_soup->id,
          $mushroom_soup->id,
          $thai_ice_tea->id,
          $lime_juice->id,
        ]);

        //bento value package
        $bento_value->dishes()->sync([
          $phad_thai_main->id,
          $seafood_pineapple_main->id,
          $seafood_fried_rice_main->id,
          $thai_basil_chicken_main->id
        ]);

        //thai value package
        $thai_value->dishes()->sync([
          $pineapple_fried_rice_main->id,
          $black_olive_rice_main->id,
          $phad_thai_main->id,
          $tomyum_fried_rice_main->id,
          $stir_fried_vegetable->id,
          $stir_fried_beansprout->id,
          $kailan_mushrooms_vegetable->id,
          $sweet_sour_fish->id,
          $cereal_fish->id,
          $thai_chilli_fish->id,
          $light_soya_fish->id,
          $honey_chicken->id,
          $deep_fried_chicken_basil->id,
          $thai_lemon_chicken_cutlet->id,
          $mango_salad_app->id,
          $papaya_salad_app->id,
          $ngoh_hiang_app->id,
          $tom_yum_seafood_app->id,
          $spring_roll->id,
          $fish_ball->id,
          $curry_samosa->id,
          $cuttle_fish_ball->id,
          $mixed_fruit_cocktail->id,
          $chin_chow->id,
          $tapioca->id,
          $red_ruby->id,
          $mango_sticky_rice->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $bandung->id,
          $thai_ice_tea_thai_value->id,
        ]);

        //thai standard package
        $thai_standard->dishes()->sync([
          $pineapple_fried_rice_main->id,
          $black_olive_rice_main->id,
          $phad_thai_main->id,
          $tomyum_fried_rice_main->id,
          $beef_fried_rice_main->id,
          $stir_fried_vegetable->id,
          $stir_fried_beansprout->id,
          $kailan_mushrooms_vegetable->id,
          $sambal_longbean_vegetable->id,
          $sweet_sour_fishprawn->id,
          $soya_fish_fishprawn->id,
          $thai_fish_fishprawn->id,
          $cereal_prawn_fishprawn->id,
          $cereal_fish_fishprawn->id,
          $blackpepper_chicken->id,
          $thai_basil_chicken_chicken->id,
          $honey_chicken->id,
          $deep_fried_chicken_basil->id,
          $thai_lemon_chicken_cutlet->id,
          $chicken_cashew_nuts->id,
          $mango_salad_app->id,
          $papaya_salad_app->id,
          $ngoh_hiang_app->id,
          $tom_yum_seafood_app->id,
          $chicken_feet_salad_app->id,
          $pandan_leaf_chicken->id,
          $spring_roll->id,
          $fish_ball->id,
          $curry_samosa->id,
          $cuttle_fish_ball->id,
          $mixed_fruit_cocktail->id,
          $chin_chow->id,
          $tapioca->id,
          $red_ruby->id,
          $mango_sticky_rice->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $bandung_thai_standard->id,
          $thai_ice_tea->id,
          $chicken_wing_chilli_signature->id,
          $prawn_cake_signature->id,
          $fish_cake_signature->id,
          $maggie_mee_signature->id,
          $green_curry_chicken_signature->id,
          $red_curry_chicken_signature->id
        ]);

        //thai gastronomy package
        $thai_gastronomy->dishes()->sync([
          $basil_leaves_rice_main->id,
          $green_curry_rice_main->id,
          $basil_leaves_rice_main->id,
          $pineapple_fried_rice_main->id,
          $black_olive_rice_main->id,
          $phad_thai_main->id,
          $seafood_fried_rice_main->id,
          $tomyum_fried_rice_main->id,
          $beef_fried_rice_main->id,
          $stir_fried_vegetable->id,
          $stir_fried_beansprout->id,
          $broccoli_mushrooms->id,
          $kailan_mushrooms_vegetable->id,
          $sambal_longbean_vegetable->id,
          $sweet_sour_fishprawn->id,
          $yellow_curry_fishprawn->id,
          $sweet_sour_prawn->id,
          $mixed_seafood_fishprawn->id,
          $soya_fish_fishprawn->id,
          $thai_fish_fishprawn->id,
          $cereal_prawn_fishprawn->id,
          $cereal_fish_fishprawn->id,
          $blackpepper_chicken->id,
          $thai_basil_chicken_chicken->id,
          $chicken_wing_thai_chilli->id,
          $honey_chicken->id,
          $deep_fried_chicken_basil->id,
          $thai_lemon_chicken_cutlet->id,
          $chicken_cashew_nuts->id,
          $mango_salad_app->id,
          $papaya_salad_app->id,
          $ngoh_hiang_app->id,
          $tom_yum_seafood_app->id,
          $chicken_feet_salad_app->id,
          $prawn_cake_app->id,
          $fish_cake_app->id,
          $pandan_leaf_chicken->id,
          $spring_roll->id,
          $fish_ball->id,
          $curry_samosa->id,
          $cuttle_fish_ball->id,
          $mixed_fruit_cocktail->id,
          $cheng_tng->id,
          $tapioca->id,
          $red_ruby_thai_gastronomy->id,
          $mango_sticky_rice_thai_gastronomy->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $bandung_thai_gastronomy->id,
          $thai_ice_tea->id,
          $thai_duck_curry_signature->id,
          $prawn_cake_signature->id,
          $fish_cake_signature->id,
          $green_curry_chicken_signature->id,
          $red_curry_chicken_signature->id
        ]);


        //asian value package
        $asian_value->dishes()->sync([
          $mee_goreng_main->id,
          $yangchow_rice_main->id,
          $pineapple_fried_rice_main->id,
          $hongkong_fried_noodle_main->id,
          $sambal_fried_rice_main->id,
          $sin_chow_bee_hoon_main->id,
          $mee_goreng_main->id,
          $mix_vege_vegetable->id,
          $luohan_vegetable->id,
          $hongkong_kailan->id,
          $sambal_longbean_vegetable->id,
          $baby_white_vegetable->id,
          $sweet_sour_fish->id,
          $cereal_fish->id,
          $breaded_fish_mayo_fish->id,
          $sambal_fish_fish->id,
          $tau_see_fish_fish->id,
          $thai_curry_chicken->id,
          $crispy_midwing_chicken->id,
          $sweet_sour_chicken->id,
          $honey_chicken->id,
          $blackpepper_chicken->id,
          $fish_cake_app->id,
          $spring_roll_app->id,
          $fish_ball_app->id,
          $curry_samosa_app->id,
          $sotong_youtiao_app->id,
          $breaded_scallop_app->id,
          $chin_chow->id,
          $almond_jelly_dessert->id,
          $red_ruby->id,
          $mango_sticky_rice->id,
          $lime_juice->id,
          $barley->id,
          $fruit_punch->id,
          $bandung->id,
          $thai_ice_tea_asian_value->id,

          ]);

          //asian standard package
          $asian_standard->dishes()->sync([
            $yangchow_rice_main->id,
            $pineapple_fried_rice_main->id,
            $hongkong_fried_noodle_main->id,
            $phad_thai_main->id,
            $tomyum_fried_beehoon_main->id,
            $sin_chow_bee_hoon_main->id,
            $hk_style_baked_rice_main->id,
            $luohan_vegetable->id,
            $oyster_kailan_mushrooms->id,
            $broccoli_cauliflower->id,
            $stir_fried_broccoli_mushrooms->id,
            $mushrooms_beancurd->id,
            $sweet_sour_fish->id,
            $spring_onion_fish_fish->id,
            $thai_chilli_fish_fish->id,
            $light_soya_celery_fish_fish->id,
            $batter_wasabi_fish_fish->id,
            $thai_red_curry_chicken_chicken->id,
            $thai_green_curry_chicken_chicken->id,
            $sweet_sour_chicken->id,
            $thai_basil_chicken_chicken->id,
            $blackpepper_chicken->id,
            $kung_pao_chicken->id,
            $thai_mango_chicken->id,
            $spring_roll_app->id,
            $chicken_ngoh_hiang_app->id,
            $sotong_youtiao_app->id,
            $breaded_scallop_app->id,
            $curry_samosa_app->id,
            $fish_cake_app->id,
            $cereal_prawn_prawn->id,
            $tempura_prawn->id,
            $thai_style_prawn->id,
            $butter_prawn->id,
            $wasabi_prawn->id,
            $bbq_sotong_thai_style->id,
            $chocolate_eclair_dessert->id,
            $vanilla_profiteroles->id,
            $chin_chow->id,
            $almond_jelly_dessert->id,
            $red_ruby->id,
            $mango_sticky_rice->id,
            $lime_juice->id,
            $barley->id,
            $fruit_punch->id,
            $bandung_asian_standard->id,
            $thai_ice_tea->id,
            $tom_yum_fish_signature->id,
            $thai_duck_curry_signature->id,
            $prawn_cake_signature->id,
            $tom_yum_signature->id,
            ]);

            //asian gastronomy package
            $asian_gastronomy->dishes()->sync([
              $pineapple_fried_rice_main->id,
              $olive_rice_anchovies_main->id,
              $tomyum_seafood_bee_hoon_main->id,
              $hk_claypot_mushroom_chicken->id,
              $nasi_briyani_main->id,
              $eefu_noodles_main->id,
              $french_beans_xo_sauce->id,
              $luo_han_pacific_clams->id,
              $broccoli_abalone_mushrooms->id,
              $asparagus_tofu_vegetable->id,
              $sayur_lodeh_tau_kwa->id,
              $hk_style_fish->id,
              $thai_chilli_fried_fish->id,
              $cereal_fish->id,
              $mango_salsa_fish->id,
              $teochew_broth_fish->id,
              $red_curry_chicken_drumstick->id,
              $green_curry_chicken_drumstick->id,
              $thai_mango_chicken_fillet->id,
              $hk_roast_chicken->id,
              $teriyaki_chicken_fillet->id,
              $battered_prawn_chilli_crab->id,
              $herbal_prawn_wolfberries->id,
              $bbq_sotong_green_chilli->id,
              $scallop_egg_white->id,
              $sambal_tunis_prawn->id,
              $deep_fried_chicken->id,
              $prawn_roll->id,
              $yam_roll_scallop->id,
              $crispy_honey_chicken->id,
              $chocolate_eclair_dessert->id,
              $vanilla_profiteroles->id,
              $red_ruby->id,
              $mango_sticky_rice->id,
              $cheesecake_dessert->id,
              $lime_juice->id,
              $barley->id,
              $fruit_punch->id,
              $bandung_asian_gastronomy->id,
              $thai_ice_tea->id,
              $bird_nest_drink->id,
              $prawn_cake_signature->id,
              $thai_duck_curry_signature->id,
              $thai_basil_chicken_signature->id,
              $thai_beef_stew_signature->id,
              $tom_yum_scallop_signature->id
            ]);


    }
}
