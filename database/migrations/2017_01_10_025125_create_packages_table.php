<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('package_id');
            $table->string('name');
            $table->float('price_pax');
            $table->integer('min_pax')->default(5);
            $table->string('type',40);
            $table->text('description');
            $table->integer('limit');
            $table->text('rules');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packages');
    }
}
