<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dishes', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('package_id');
            $table->string('name');
            $table->string('section');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dishes');
    }
}
// class Dish extends Model
// {
//   protected $table = "dishes";
//   protected $hidden = [];
//   protected $dates = ["created_at","updated_at"];
//   protected $fillable = ["package_id","name","section"];
//   protected $guarded = [];
// }
