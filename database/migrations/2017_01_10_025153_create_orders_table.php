<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ["user_id","package_id","selection","total_amount","delivery_date","paid"];
      Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('order_id')->unsigned()->index();
            $table->text('select_package');
            $table->text('salad');
            $table->text('sandwich');
            $table->text('addons');
            $table->integer('min_pax');
            $table->float('total_amount');
            $table->boolean('paid')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('orders');
    }
}
