<?php

return [
  'sections' => [
    'main' => 'Main Course',
    'sidedish' => 'Side Dishes',
    'vegetable' => 'Vegetable',
    'fish' => 'Fish',
    'prawn' => 'Prawn',
    'fishprawn' => 'Fish and Prawn',
    'seafood' => 'Seafood',
    'chicken' => 'Chicken',
    'appetizer' => 'Appetizer',
    'fingerfood' => 'Finger Food',
    'dessert' => 'Dessert',
    'beverage' => 'Beverage',
    'signaturedish' => 'Signature Dishes',
    'soup' => 'Soup',
    'salad' => 'Salad',
    'sandwich' => 'Sandwich',
    'dimsum' => 'Dim Sum',
    'pasteries' => 'Pasteries',
    'kueh' => 'Kueh'
  ],
  'heading' => ['selection'=>'Select A Package'],

  'addons' => [
    'title' => 'Add-on Side Orders',
    'one' => 'Thai Roasted Duck Curry',
    'two' => 'Thai Green Curry',
    'three' => 'Thai Red Curry',
    'four' => 'Tom Yum Fried Bee Hoon',
    'five' => 'Signature Phad Thai Noodles'
  ],

  'postalcodes' => [1,2,3,4,5,6,7,8,9,62,63],
  // enter in ddmmyyyy
  'public_holidays' => [01012017,28012017,29012017,14042017,01052017,10052017,25062017,09082017,01092017,18102017,25122017],

  'catering_times' => ['0900 hrs','1000 hrs','1100 hrs','1200 hrs','1300 hrs','1400 hrs','1500 hrs','1600 hrs','1700 hrs','1800 hrs','1900 hrs','2000 hrs','2100 hrs'],
];
