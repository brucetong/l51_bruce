var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
    .styles(['jquery.mmenu.all.css'])
    .scripts(['jquery.mmenu.all.min.js'])
    .version(["public/templates/mmenu/js/jquery.mmenu.all.min.js",
    "public/templates/mmenu/css/jquery.mmenu.all.css"]);
});
