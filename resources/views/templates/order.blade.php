<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

  <style>
  body{
    /*background-image:url('https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-22-min.jpeg');*/
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }

  label{
    display:block;
  }

  .opacity-bar{
       border: none;
       margin-bottom:0px;
       border-radius: 0;
       background-color: rgba(0,0,0,0.8);
   }
   .navbar-header{
    display: table;
    width: 100%;
    padding: 0 3%;
    margin: 0 auto;
   }
   .box{
    display: table;
    margin: 0 auto;
    vertical-align:middle;
    float:none;
   }
  .jj-spacer{
    padding:2% 0;
  }
  #my-page{
    background-color:#ffffff;
    display: table;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 20px;
  }
  #top-bar{
    background-color:#CE8800;
  }
  .blur{
    box-shadow:2px 2px 2px 1px rgba(0, 0, 0, 0.2);
  }
  .footer{
    display: table;
    background-color: #eeeeee;
    min-height: 50px;
    width: 100%;
  }
  /*side bar navigation*/
  .nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 15px;
  font-weight: 200;
  background-color: #ffb21c;
  opacity: 0.8;
  top: 0px;
  width: 100%;
  height: 100%;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #ffb21c;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 15px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: 'Glyphicons Halflings';
  content: "\e258";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #efaf00;
  background-color: #efaf00;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #ffb21c;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #efaf00;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #efaf00;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: 'Glyphicons Halflings';
  content: "\e258";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 10px;
  border-left: 3px solid #efaf00;
  border-bottom: 1px solid #efaf00;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #a25716;
  font-weight:bold;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 30px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #efaf00;
  background-color: #efaf00;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
    color:#a25716;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}
body {
  margin: 0px;
  padding: 0px;
}

  </style>

</head>
<header>

  <nav class="navbar navbar-inverse opacity-bar">
    <div class="container-fluid">
      <div class="navbar-header">
        <div class="box">
          <a class="navbar-brand jj-spacer" href="#">
            <img alt="Brand" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/Witcharut-logo-round.png" height="40px">
          </a>
        </div>
      </div>
    </div>
  </nav>

</header>

<div id="my-page">

<div class="container">

  <div class="row">

    <div class="col-md-3">
      <div class="nav-side-menu">
        <div class="brand">Catering Menu</div>
          <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

          <div class="menu-list">

            <ul id="menu-content" class="menu-content collapse out">

                {{--<li>
                  <a href="#">
                  <i class="fa fa-dashboard fa-lg"></i> Dashboard
                  </a>
                </li>--}}

                <li>
                  <a href="#"><i class="fa fa-gift fa-lg"></i>Bento Standard<span class="arrow"></span></a>
                </li>

                {{--<ul class="sub-menu collapse" id="products">
                    <li class="active"><a href="#">CSS3 Animation</a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#">Buttons</a></li>
                    <li><a href="#">Tabs & Accordions</a></li>
                    <li><a href="#">Typography</a></li>
                    <li><a href="#">FontAwesome</a></li>
                    <li><a href="#">Slider</a></li>
                    <li><a href="#">Panels</a></li>
                    <li><a href="#">Widgets</a></li>
                    <li><a href="#">Bootstrap Model</a></li>
                </ul>--}}


                <li data-toggle="collapse" data-target="#service" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i>High Tea Package<span class="arrow"></span></a>
                </li>

                {{--<ul class="sub-menu collapse" id="service">
                  <li>New Service 1</li>
                  <li>New Service 2</li>
                  <li>New Service 3</li>
                </ul>--}}


                <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i>Bento Value<span class="arrow"></span></a>
                </li>

                <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i>Thai Gastronomy<span class="arrow"></span></a>
                </li>

                <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i>Thai Standard<span class="arrow"></span></a>
                </li>

                <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-gift fa-lg"></i>Thai Value<span class="arrow"></span></a>
                </li>

            </ul>
     </div>
</div><!--side nav-->
</div><!--col-->



    <div class="col-md-6">
    <div class="form-group">

      <div class="panel panel-default">
        <div class="panel-heading">Bento Package</div>
          <div class="panel-body">
            <div class="radio">
              <label>
                <input type="radio" id="optionsRadios1" name="optionsRadios" value="option1">
                Option one is this
              </label>
              <label>
                <input type="radio" id="optionsRadios2" name="optionsRadios" value="option2">
                Option two is this
              </label>
              <label>
                <input type="radio" id="optionsRadios3" name="optionsRadios" value="option3">
                Option three is this
              </label>
            </div><!--radio-->
        </div><!--panel body-->
      </div><!--panel default-->
    </div>

    <div class="form-group">
      <div class="panel panel-default">
        <div class="panel-heading">Bento Package</div>
          <div class="panel-body">
            <div class="radio">
              <label>
                <input type="radio" id="optionsRadios4" name="optionsRadios2" value="option4">
                Option one is this
              </label>
              <label>
                <input type="radio" id="optionsRadios5" name="optionsRadios2" value="option5">
                Option two is this
              </label>
              <label>
                <input type="radio" id="optionsRadios6" name="optionsRadios2" value="option6">
                Option three is this
              </label>
            </div><!--radio-->
        </div><!--panel body-->
      </div><!--panel default-->


    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Additional Instructions</h3>
      </div>

      <div class="panel-body">
        <textarea class="form-control" rows="3" placeholder="Type Your Instructions"></textarea>
      </div>
    </div><!--panel-->
  </div><!--form-->
</div><!--col-->



<div class="col-md-3">

<div class="well well-lg">


</div>

</div>

</div><!--row-->


</div><!--container-->

</div><!--page-->




<footer class="footer">
  <div class="container">

  </div>
</footer>
</html>
