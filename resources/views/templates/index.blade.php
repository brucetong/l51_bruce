<!DOCTYPE html>
<html>
  <head>
    <title>Index Page for High Tea</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

  </head>
  <body>
    <div class="container">
    <h2 class="text-center text-uppercase">Orders</h2>

    @foreach ($pages as $page)
      <table class="table">
      <thead>
      <th><a href="{{ action('PagesController@show', [$page->id]) }}">{{ $page->id }}. {{ $page->title }}</a></th>
      </thead>
      <tr>
          <td> {{ $page->selection }} </td>
          <td> {{ $page->salad }} </td>
          <td> {{ $page->sandwich }} </td>
          <td> {{ $page->chicken }} </td>
          <td> {{ $page->dimsum }} </td>
          <td>$ {{ $page->price }}</td>
          <td><a href="#">Remove</a></td>
          <td><a href="#">Edit Order</a></td>
      </tr>
      </div>
    </table>
    @endforeach
    <button type="button" class="btn btn-success btn-lg">Pay Now <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></button>
  </div>
  </body>
</html>
