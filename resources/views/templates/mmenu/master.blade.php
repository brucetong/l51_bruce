<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="author" content="www.frebsite.nl" />
		<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />

		<title>jQuery.mmenu demo</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.0/css/bootstrap.min.css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.1/css/font-awesome.css" media="screen" charset="utf-8">

		<link rel="stylesheet" href="{{ asset('templates/mmenu/css/demo.css') }}">
		<link rel="stylesheet" href="{{ asset('templates/mmenu/css/jquery.mmenu.all.css') }}">


    <link rel="stylesheet" href="{{ asset('css/overrides.css') }}">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js" charset="utf-8"></script>

		<script src="{{ asset('templates/mmenu/js/jquery.mmenu.all.min.js') }}"></script>
		<script type="text/javascript">
			$(function() {
				$('nav#menu').mmenu();
			});
		</script>
	</head>
	<body>
		<div id="page">
			<div class="header">
				<a href="#menu"><span></span></a>
				Demo
			</div>
			<div class="content">
				<p><strong>This is a demo.</strong><br />
					Click the menu icon to open the menu.</p>
			</div>
			<nav id="menu">
				<ul>
					<li><a href="#">Home</a></li>
					<li><span>About us</span>
						<ul>
							<li><a href="#about/history">History</a></li>
							<li><span>The team</span>
								<ul>
									<li><a href="#about/team/management">Management</a></li>
									<li><a href="#about/team/sales">Sales</a></li>
									<li><a href="#about/team/development">Development</a></li>
								</ul>
							</li>
							<li><a href="#about/address">Our address</a></li>
						</ul>
					</li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</nav>
		</div>
	</body>
</html>

{{--


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('templates/mmenu/js/jquery.mmenu.all.min.js') }}"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('templates/mmenu/css/demo.css') }}">
<link rel="stylesheet" href="{{ asset('templates/mmenu/css/jquery.mmenu.all.css') }}">
<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

<div id="my-page">

<div class="container">

<nav id="my-menu">

  <ul>
   <li class="spacer"><a href="/">Home</a></li>
   <li class="spacer"><span>Ordering Menu</span></li>
    <ul>
      <a href="#">Thai Gastronomy Menu</a>
      <a href="#">Thai Standard Menu</a>
      <a href="#">Asian Gastronomy Menu</a>
      <a href="#">Asian Standard Menu</a>
    </ul>
   <li class="spacer"><a href="/promotions/">Promotions</a></li>
 </ul>

</nav>

    <div class="row">

          <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJTHAIFOOD-17-resized.jpg" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJTHAIFOOD-17-resized.jpg" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJTHAIFOOD-17-resized.jpg" alt=""></div>
    </div>

  </div>

</div>
--}}
