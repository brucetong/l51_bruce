<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

  <style>

   .box{
    display: table;
    margin: 0 auto;
    vertical-align:middle;
    float:none;
   }
  .jj-spacer{
    padding:2%;
    width:100%;
    margin:0 auto;
  }
  #my-page{
    background-color:#ffffff;
    display: table;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 20px;
  }

  .footer{
    display: table;
    background-color: #eeeeee;
    min-height: 50px;
    width: 100%;
  }

  .min-height{
    min-height:54px;
  }

  #page{
    max-width:1440px;
    margin:0 auto;
  }

  .outer{
    display:block;
    margin-bottom:30px;
  }

  .main{
    background-color:#222222;
  }

  .navbar{
    border-radius:0px;
    margin-bottom:10px;
  }
  .background-grey{
    display:block;
    width:100%;
    height:292px;
    background-color:#cccccc;
  }

  .form-group div {
    padding:2px 0;
  }

  </style>

</head>

<body>

<div class="container-fluid">

  <div id="page">

    <div class="outer">

    <nav class="navbar navbar-inverse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Witcharut Catering <span class="sr-only">(current)</span></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Asian</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Thai</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Bento</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">High Tea</a></li>
          </ul>
        </li>
        <li><a href="#">My Account</a></li>
        <li><a href="#">Shopping Cart</a></li>
      </ul>
    </nav>

    {{--Columns--}}

    <h2 class="text-center text-uppercase">High Tea Package</h2>

    <hr>

    <div class="row">

            <div class="col-md-4 col-xs-12 pull-left"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJTHAIFOOD-17-resized.jpg" alt="">
            <div class="panel"><div class="panel-heading"><div class="box text-center">Asian Gastronomy<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
            </div></div></div>
            <div class="col-md-4 col-xs-12"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-24-min-resized.jpeg" alt="">
            <div class="panel"><div class="panel-heading"><div class="box text-center">Thai Gastronomy<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
            </div></div></div>
            <div class="col-md-4 col-xs-12 pull-right"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-25-min-resized.jpeg" alt="">
            <div class="panel"><div class="panel-heading"><div class="box text-center">Bento Value<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
            </div></div></div>

    </div>{{--row--}}


            <div class="row">
              <div class="col-md-9 col-xs-12">
                <div class="container-fluid">

                {{--action url foo/bar--}}
                {!! Form::open(['url' => 'high_tea/create','files'  =>  true]) !!}


                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
                  <div class="alert alert-danger">
                  <p class="lead">Your order was not saved</p>
                  </div>
              @endif

                @if (session('status'))
                  <div class="alert alert-success">
                    <p class="lead">{{ session('status') }}</p>
                  </div>
                @endif
                {{--Form:open(['method'=>'PATCH', 'action'=>['PageController@update',$article->id]])--}}
                {!! Form::open() !!}

                <div class="container-fluid">
                  <div class="form-group">
                    {!! Form::hidden('title', 'high tea package') !!}

                    <h3>Selection</h3>
                    <div class="col-md-4">
                    {!! Form::radio('selection', '100 pax') !!} $3.00 Minimum. 100 pax (3 dishes + 1 drink)
                    </div>
                    <div class="col-md-4">
                    {!! Form::radio('selection', '80 pax') !!} $4.00 Minimum. 80 pax (3 dishes + 1 drink)
                    </div>
                    <div class="col-md-4">
                    {!! Form::radio('selection', '5 dishes 1 drink') !!} $5.00 Minimum. (5 dishes + 1 drink)
                    </div>
                    <div class="col-md-4">
                    {!! Form::radio('selection', '6 dishes 1 drink') !!} $6.00 Minimum. (6 dishes + 1 drink)
                    </div>
                    <div class="col-md-4">
                    {!! Form::radio('selection', '7 dishes 1 drink') !!} $7.00 Minimum. (7 dishes + 1 drink)
                    </div>
                    </br>
                    <div class="col-md-12">
                    <h5>**Free Hot Coffee and Tea</h5>
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                  </div>
                </div>


                <div class="container-fluid">

                  <div class="form-group">

                    <h3>Salad</h3>
                    <div class="col-md-5">
                    {!! Form::radio('salad', 'caesar salad') !!} Caesar Salad w/Croutons Fruit Salad
                    </div>
                    <div class="col-md-7">
                    {!! Form::radio('salad', 'mesculin mix salad') !!} Mesculin Mix Salad w/Honey Mustard Dressing
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('salad', 'thai mango salad') !!} Thai Mango Salad
                    </div>
                    <div class="col-md-9">
                    {!! Form::radio('salad', 'potato salad') !!} Potato Salad
                    </div>

                    <div class="clearfix visible-xs-block"></div>
                  </div>

                </div>

                <div class="container-fluid">

                  <div class="form-group">

                    <h3>Sandwich</h3>
                    <div class="col-md-2">
                    {!! Form::radio('sandwich', 'egg mayo') !!} Egg Mayo
                    </div>
                    <div class="col-md-2">
                    {!! Form::radio('sandwich', 'tuna mayo') !!} Tuna Mayo
                    </div>
                    <div class="col-md-2">
                    {!! Form::radio('sandwich', 'vegan') !!} Vegan
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('sandwich', 'curry potato') !!} Curry Potato
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('sandwich', 'egg ham cheese') !!} Egg Ham and Cheese
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                  </div>

                </div>

                <div class="container-fluid">

                  <div class="form-group">

                    <h3>Chicken</h3>
                    <div class="col-md-3">
                    {!! Form::radio('chicken', 'bbq chicken mid wing') !!} BBQ Chicken Mid Wing
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('chicken', 'chicken nuggets') !!} Chicken Nuggets
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('chicken', 'seaweed chicken roll') !!} Seaweed Chicken Roll
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('chicken', 'teriyaki mid wing') !!} Teriyaki Mid Wing
                    </div>
                    <div class="col-md-12">
                    {!! Form::radio('chicken', 'crispy drumlet') !!} Crispy Drumlet
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                  </div>
                </div>

                <div class="container-fluid">

                  <div class="form-group">

                    <h3>Dim Sum</h3>
                    <div class="col-md-3">
                    {!! Form::radio('dimsum', 'siew mai') !!} Steamed Siew Mai
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('dimsum', 'har kau') !!} Steamed Har Kau
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('dimsum', 'yam cake') !!} Mini Yam Cake
                    </div>
                    <div class="col-md-3">
                    {!! Form::radio('dimsum', 'red bean pau') !!} Mini Red Bean Pau
                    </div>
                    <div class="col-md-9">
                    {!! Form::radio('dimsum', 'char siew chicken pau') !!} Mini Char Siew Chicken Pau
                    </div>

                    </div>

              </div>

                <div class="container-fluid" style="padding-top:4%;">
                  <div class="form-group">

                    {!! Form::submit('Submit Order', ['class'=>'btn btn-success form-control']); !!}

                  </div>
                </div>

                {!! Form::close() !!}

                </div>{{--container fluid--}}
              </div>{{--col 9--}}

              <div class="col-md-3 col-xs-12">
                <div class="container-fluid">
                  <div class="background-grey">
                    <h2 class="text-center" style="vertical-align:middle;line-height:280px;">Ad</h2>
                  </div>
                  <div class="background-grey">
                    <h2 class="text-center" style="vertical-align:middle;line-height:280px;">Ad</h2>
                  </div>
                </div>
              </div>{{--col 4--}}
            </div>
      </div>{{--outer--}}

  </div>{{--page--}}

</div>{{--container fluid--}}

</body>

{{--footer--}}
<footer class="footer">

  <div class="container-fluid jj-spacer">

    <div class="col-xs-12">

    <div class="row">

      <div id="page">

    <address>
      <strong>Witcharut Catering</strong><br>
      8A Admiralty Street, #06-40<br>
      Food Xchange, Singapore 757437<br>
      <abbr title="Phone">P:</abbr>
      <a href="mailto:#"></a>
    </address>

    <address>
      <strong>JJ Thai</strong><br>
      8 Jalan Legundi<br>
      #01-10, Victory 8
      Singapore 759274<br>
      <abbr title="Phone">P:</abbr>
      <a href="mailto:#"></a>
    </address>

    <address>
      <strong>Makan Thai</strong><br>
      8 Jalan Legundi<br>
      #01-05, Victory 8
      Singapore 759274<br>
      <abbr title="Phone">P:</abbr>
      <a href="mailto:#"></a>
    </address>

      </div>

    </div>{{--row--}}

    </div>{{--col--}}


  </div>


</footer>

</html>
