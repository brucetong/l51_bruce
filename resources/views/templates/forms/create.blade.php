@extends('wapp')

@section('content')

<div class="container-fluid">
  <div class="caption">
    <h4 class="pull-right">$20.00 </h4>
    <h1>Fill In Your Order Details</h1>
  </div>

  {{--action url foo/bar--}}
  {!! Form::open(['url' => 'foo/bar','files'  =>  true]) !!}



    <div class="form-group">

      {!! Form::label('name', 'Name:') !!}

      {!! Form::text('name', null, ['class' => 'form-control', 'foo'  => 'bar ']) !!}

    </div>


    <div class="form-group">

      {!! Form::label('body', 'Body:') !!}

      {!! Form::textarea('body', null, ['class'=>'form-control']) !!}

    </div>


    <div class="form-group">

      {!! Form::radio('name', 'value'); !!}

      {!! Form::label('email', 'Weekdays', ['style'=>'padding:10px']); !!}

      {!! Form::radio('name', 'value'); !!}

      {!! Form::label('email', 'Weekends'); !!}

    </div>

    <div class="form-group">

      {!! Form::submit('Add Order', ['class'=>'btn btn-success form-control']); !!}

    </div>

  {!! Form::close() !!} 

</div>


@stop
