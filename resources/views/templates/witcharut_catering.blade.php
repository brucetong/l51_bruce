<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

</head>

  <style>
  body{
    /*background-image:url('https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-22-min.jpeg');*/
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }
  .opacity-bar{
       border: none;
       margin-bottom:0px;
       border-radius: 0;
       background-color: rgba(0,0,0,0.8);
   }
   .navbar-header{
     display: table;
    width: 100%;
    padding: 0 3%;
    margin: 0 auto;
   }
   .box{
    display: table;
    margin: 0 auto;
    vertical-align:middle;
    float:none;
   }
  .jj-spacer{
    padding:2%;
    width:100%;
    margin:0 auto;
  }
  #my-page{
    background-color:#ffffff;
    display: table;
    margin: 0 auto;
    box-sizing: border-box;
    padding: 20px;
  }
  #top-bar{
    background-color:#CE8800;
  }
  .blur{
    box-shadow:2px 2px 2px 1px rgba(0, 0, 0, 0.2);
  }
  .footer{
    display: table;
    background-color: #eeeeee;
    min-height: 50px;
    width: 100%;
  }

  .min-height{
    min-height:54px;
  }
  </style>

</head>

<header>

  <nav class="navbar navbar-inverse opacity-bar">
    <div class="container-fluid min-height">

      <div class="navbar-header">
        <div class="col-md-4"></div>
          <div class="col-md-4">
            <a class="navbar-brand jj-spacer" href="#"><img alt="Brand Logo" style="margin:0 auto;" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/Witcharut-logo-round.png" height="40px"></a>
          </div>
          <div class="col-md-4"><ul class="nav navbar-nav navbar-right"><li><a class="btn btn-primary" style="color:#efffff;" href="#">Order Now</a></li></div>
      </div>

    </div>
  </nav>

  <img src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-2560-min.jpeg" width="100%" alt="">

</header>
<body>
<div id="my-page">

<div class="container">

  <div class="row text-center">

    <h1>Catering Menu</h1>
    <p class="jj-spacer">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse congue tellus massa, eu placerat ligula porttitor quis. Mauris ac nisl et elit pharetra porttitor sit amet sed nisi. Praesent malesuada finibus est, sed sodales metus elementum quis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sit amet tempus augue. </p>

  </div>
{{--Columns--}}
  <div class="row">

          <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJTHAIFOOD-17-resized.jpg" alt="">
          <div class="panel"><div class="panel-heading"><div class="box text-center">Asian Gastronomy<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
          </div></div></div>
          <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-24-min-resized.jpeg" alt="">
          <div class="panel"><div class="panel-heading"><div class="box text-center">Thai Gastronomy<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
          </div></div></div>
          <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-25-min-resized.jpeg" alt="">
          <div class="panel"><div class="panel-heading"><div class="box text-center">Bento Value<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
          </div></div></div>
    </div>

    <div class="row">

            <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-28-min-resized.jpeg" alt="">
            <div class="panel"><div class="panel-heading"><div class="box text-center">Asian Gastronomy<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
            </div></div></div>
            <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-24-min-resized.jpeg" alt="">
            <div class="panel"><div class="panel-heading"><div class="box text-center">Thai Gastronomy<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
            </div></div></div>
            <div class="col-md-4"><img class="img-responsive" src="https://s3-ap-southeast-1.amazonaws.com/witcharut-templates/JJThaiHouse_Dishes-25-min-resized.jpeg" alt="">
            <div class="panel"><div class="panel-heading"><div class="box text-center">Bento Value<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
            </div></div></div>
      </div>

  </div>

</div>
</body>
<footer class="footer">

  <div class="container">
    <div class="row">
      <address>
      <strong>Witcharut Catering</strong><br>
      8A Admiralty Street, #06-40<br>
      Food Xchange, Singapore 757437<br>
      <abbr title="Phone">P:</abbr>
    </address>

    <address>
      <strong>Barry Han</strong><br>
      <a href="mailto:#">first.last@example.com</a>
    </address>
    </div>

  </div>

</footer>
</html>
