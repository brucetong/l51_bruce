<!DOCTYPE html>
<html>
  <head>
    <title>Index Page for High Tea</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

  </head>
  <body>

    <div class="container">

      <h2 class="text-center text-uppercase">Orders</h2>
      <p class="text-center"><a class="lead" href="{{ URL::previous() }}">Go Back To Previous Page</a></p>

        <table class="table">

          <thead>
          <th>{{ $page->title }}</th>
          </thead>

          <tr>
            <td>{{ $page->selection }}</td>
            <td>{{ $page->salad }}</td>
            <td>{{ $page->sandwich }}</td>
            <td>{{ $page->chicken }}</td>
            <td>{{ $page->dimsum }}</td>
            <td>$ {{ $page->price }}</td>
          </tr>

        </table>
    </div>

  </body>
</html>
