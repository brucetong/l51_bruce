<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create A Card</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="container-fluid">
    <form class="col-sm-3" action="{{ route('send-html-email') }}" method="post">

      {!! csrf_field() !!}

    <div class="form-group">

      <label for="name">Enter Name</label>

      <input type="text" class="form-control" id="name" name="name" value="" placeholder="Enter Name">

    </div><!-- form group -->

    <div class="form-group">

      <label for="body">Enter Message Body</label>

      <textarea class="form-control" name="body" id="body" value="" autofocus="yes" rows="5" cols="30" placeholder="Type Your Message Body"></textarea>

    </div><!-- form group -->

    <div class="form-group">

      <button type="submit" class="btn btn-info btn-lg" name="submit">Submit this form</button>

    </div><!-- form group -->
    
    </form>
  </div>
  </body>
</html>
