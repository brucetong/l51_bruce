<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>Create A Card</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  </head>
  <body>

    <form class="" action="{{ route('post-card-create') }}" method="post">

    <div class="form-group">

      <label for="title">Enter Any Title</label>

      <textarea class="form-control" name="title" id="title" value="" autofocus="yes" rows="5" cols="30">Create A Card</textarea>

    </div>

    <div class="form-group">

      <button type="submit" class="btn btn-info btn-lg" name="submit">Create Card</button>

    </div>

    </form>

  </body>
</html>
