<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Delete A Note</title>
  </head>
  <body>
    <form class="" action="{{ route('post-card-delete') }}" method="post">
      {!! csrf_field() !!}
      <label for="card">Choose A Card To Delete</label>

      <select id="card" class="" name="id">

      @foreach($delete_cards as $card)

        <option value="{{ $card->id }}">{{ $card->title }}</option>

      @endforeach

      </select>
      <button type="submit" name="submit">Delete Card</button>
    </form>
  </body>
</html>
