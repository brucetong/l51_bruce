<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>Create A Card</title>
    <h1>Update A Record</h1>

  </head>
  <body>

    <table>

      @foreach( $update as $updates)
        {{ $updates->id }}. {{ $updates->title }}</br>
      @endforeach

    </table>


    <form enctype="multipart/form-data" class="" action="{{ route('post-card-update') }}" method="post">

      {!! csrf_field() !!}

      <select class="" id="update" name="card_id">

        @foreach($update as $updatecard)

        <option value="{{ $updatecard->id }}">{{ $updatecard->id }}. {{ $updatecard->title }}</option>

        @endforeach

      </select>

      <label for="title">Enter Your New Title</label>

      <input id="title" type="text" name="title" value="">

      <button type="submit" name="submit">Submit</button>

  </form>

  </body>
</html>
