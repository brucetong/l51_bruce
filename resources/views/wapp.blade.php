<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
{{-- <link rel="stylesheet" href="{{ URL::asset('bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker.min.css') }}"> --}}
<title>@yield('title')</title>

<style>


  .w-vertical-gap{
   margin-top:1%;
   margin-bottom:1%;
  }


  .footer{
   display: table;
   /*background-color: #ffd700;*/
   min-height: 50px;
   width: 100%;
  }


  #outer{
   max-width:1200px;
   margin:0 auto;
  }

  .page{
   background-color:#ffffff;
   display:block;
   padding-bottom:40px;
   min-height: 1000px;
   height: 100%;
  }

  .main{
   background-color:#222222;
  }


  .form-group div {
   padding:5px 5px;
   font-family: 'Open Sans', sans-serif;
  }

  body{
   font-size:15px;
   font-family: 'Open Sans', sans-serif;
  }


  h4 {
   font-family: 'Open Sans', sans-serif;
   font-weight: 400;
   line-height:1.5;
   font-size:16px;
  }


  .page-outer{
   display:block;
   background-color:#fafafa;
  }

  .alert-danger {
   font-size: 21px;
   line-height: 1.4;
  }

  .alert-success {
   font-size: 21px;
   line-height: 1.4;
  }

  p.package-title {
   font-size: 19px;
   font-family:'Maven Pro', sans-serif;
   line-height: 1.5;
   display: block;
   padding: 1%;
   background-color: gold;
   text-align: center;
   border-radius: 4px;
  }

  .addons-labels{
   font-size:21px;
   vertical-align:top;
  }

  .lead {
   font-size:19px;
  }

  .calendar {
    width: 100%;
    border-collapse: collapse;
    margin-top: 20px;
  }

  .calendar caption {
    margin-bottom: 10px;
  }

  .calendar .header {
    background: #555;
    color: #fff;
  }

  .calendar tr:nth-child(odd) {
    background: #f0f0f0;
  }

  .calendar tr:nth-child(even) {
    background: #e0e0e0;
  }

  .calendar th,
  .calendar td {
    padding: 15px;
    border: 1px solid #eee;
  }

</style>

</head>
<body>


  <div class="page-outer">
    <div class="container-fluid">
        <div id="outer">

            <div class="page">@yield('content')

        </div>
    </div>
  </div>


<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
{{-- <script src="{{ URL::asset('bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js') }}"></script> --}}
{{-- <script src="{{ URL::asset('bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker.en-GB.min.js') }}"></script> --}}

<script>
$(document).ready(function(){

  $('#container .input-daterange').datepicker({
    startDate: '+3d',
    })
})
</script>
@yield('scripts')
</body>
</html>
