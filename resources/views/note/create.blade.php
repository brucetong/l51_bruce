<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Create A Note</title>
  </head>
  <body>
    <form class="" action="{{ route('post-note-create') }}" method="post">
      {!! csrf_field() !!}
      <label for="body">Enter A Note</label>
      <textarea id="body" name="body" rows="8" cols="80"></textarea>
      <label for="card">Select A Card</label>
      <select id="card" class="" name="card">
        @foreach($cards as $card)
        <option value="{{ $card->id }}">{{ $card->title }}</option>
      @endforeach
      </select>
      <button type="submit" name="submit">Submit Note</button>
    </form>
  </body>
</html>
