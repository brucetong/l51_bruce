<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Delete A Note</title>
  </head>
  <body>
    <form class="" action="{{ route('post-note-delete') }}" method="post">
      {!! csrf_field() !!}
      <label for="note">Select A Card</label>
      <select id="note" class="" name="note">
        @foreach($ as $card)
        <option value="{{ $notes->id }}">{{ $card->title }}</option>
      @endforeach
      </select>
      <button type="submit" name="submit">Delete Note</button>
    </form>
  </body>
</html>
