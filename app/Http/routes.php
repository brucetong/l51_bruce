<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('date/index/',array(
  'as' => 'dates.index',
  'uses'=> 'DateSelectController@index'
));

// insert {id?} to solve a minor issue where a '?' is inserted after index/ when user clicks on 'Try Again' if no vacant dates are available

Route::post('date/create',array(
  'as'=>'dates.create',
  'uses'=>'DateSelectController@postMyDatePicker'
));

Route::post('date/store',array(
  'as' => 'dates.store',
  'uses' => 'DateSelectController@postDateCreate'
));

Route::get('date/{id}/edit',array(
  'as' => 'dates.edit',
  'uses' => 'DateSelectController@editDate'
));

Route::patch('date/{id}',array(
  'as' => 'dates.update',
  'uses' => 'DateSelectController@updateDate'
));

Route::delete('date/{id}',array(
  'as' => 'dates.destroy',
  'uses' => 'DateSelectController@destroyDate'
));


/************************/


Route::get('test/', array(
  'as' => 'test',
  'uses' => 'TestController@index'
));


Route::post('test/',array(
  'as' => 'mydatetimefunction',
  'uses' => 'TestController@mydatetimefunction'
));

Route::post('savepost/',array(
  'as' => 'store_date',
  'uses' => 'TestController@storedate'
));


/*
|--------------------------------------------------------------------------
| Catering Website below this line
|--------------------------------------------------------------------------
|
*/
  Route::get('index',array(
    'as' => 'index',
    'uses' => 'CateringController@index'
  ));
  Route::post('flush-data',array(
    'as'=>'postFlushData',
    'uses'=>'CateringController@flushData'
  ));
  Route::get('flush-data','CateringController@flush');


  Route::get('high-tea','HighTeaController@showAll');
  Route::get('high-tea/{id}', 'HighTeaController@index');
  //we POST and pass the id to controller
  Route::post('high-tea/order/{id}',array(
    'as'=>'postHighTea',
    'uses'=>'HighTeaController@store'
  ));
  Route::get('asian-dish/{id}','AsianPackageController@index');
  Route::post('asian-dish/{id}',array(
    'as'=>'postAsianDish',
    'uses'=>'AsianPackageController@store'
  ));
  Route::get('thai-dish/{id}','ThaiPackageController@index');
  Route::post('thai-dish/{id}',array(
    'as'=>'postThaiFood',
    'uses'=>'ThaiPackageController@store'
  ));
  Route::get('bento-set/{id}','BentoPackageController@index');
  Route::post('bento-packages/{id}',array(
    'as'=>'postBentoSet',
    'uses'=>'BentoPackageController@store'
  ));
  Route::get('checkout/addon_dishes/',array(
    'as' => 'addon_dishes',
    'uses' => 'CateringController@getCheckOutAddon'
  ));
  Route::post('checkout/addon_dishes/',array(
  'as' =>'addon_order',
  'uses'=>'CateringController@postCheckOutAddon'
  ));
  Route::get('checkout/userinfo','CateringController@getUserInfo');
  Route::post('checkout/userinfo',array(
  'as' =>'userinfo',
  'uses'=>'CateringController@postUserInfo'
  ));
  Route::get('/cart',array(
    'as' => 'cart',
    'uses' => 'CateringController@getCart'
  ));
  Route::get('/checkout',array(
    'as' => 'checkout',
    'uses' => 'CateringController@getCheckout'
  ));
  Route::post('/checkout',array(
    'as' => 'checkout',
    'uses' => 'CateringController@postCheckout'
  ));
  Route::get('/checkout-error', array(
    'as' => 'checkout-error',
    'uses' => 'CateringController@getCheckoutError'
  ));
// Route::get('/high_tea_package',[
//   'as'=>'savepackage',
//   'uses'=>'HighTeaController@index'
// ]);

// Route::post('/catering/package/hightea',array(
//   'as'=>'savepackage',
//   'uses'=>'HighTeaController@saveNewPackage'
// ));
//
// Route::get('/catering/package/hightea',[
//   'as'=>'savepackage',
//   'uses'=>'HighTeaController@create'
// ]);

/*
|--------------------------------------------------------------------------
| Catering Website above this line
|--------------------------------------------------------------------------
|



Route::get('test', [
  'as' => 'test',
  'uses' => 'TestController@index'
]);

Route::get('witcharut-about','PagesController@about');

Route::get('mmenu',function(){
    return view('templates.mmenu.master');
});

Route::get('witcharut_catering',function(){
    return view('templates.witcharut_catering');
});

Route::get('high_tea/index','PagesController@index');

Route::get('high_tea/index/{id}', 'PagesController@show');

Route::post('high_tea/create','PagesController@store');

Route::get('high_tea/create','PagesController@create');

Route::get('ordering_template',function(){
    return view('templates.order');
});

Route::get('sendhtml', 'EmailsController@show_html_form');

Route::post('sendhtml', array(
  'as'  =>  'send-html-email',
  'uses'  =>  'EmailsController@html_email'
));

//Route::get('sendhtmlemail', 'EmailsController@html_email');

Route::get('send_test_email', function(){
	Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
	{
    $message->subject('Mailgun and Laravel are awesome!');
		$message->from('no-reply@website_name.com', 'Website Name');
		$message->to('tongcheong77@gmail.com');
	});
});

Route::get('cards',  'CardsController@index');

Route::get('card/{id}',  'CardsController@show');

Route::get('cards/create', 'CardsController@create');

Route::get('cards/update', array(
  'as'  =>  'update-list',
  'uses'  =>  'CardsController@updatelist'
));

Route::post('cards/update', array(
  'as'  =>  'post-card-update',
  'uses'  =>  'CardsController@update'
));

Route::get('cards/delete', 'CardsController@deletecards');

Route::post('cards/delete', array(
  'as'  =>  'post-card-delete',
  'uses'  =>  'CardsController@destroy'
));

Route::post('cards/create', array(
  'as' 		=> 'post-card-create',
	'uses'	=> 'CardsController@store'
));

Route::get('notes/create', 'NotesController@create');

Route::post('notes/create', array(
  'as' 		=> 'post-note-create',
	'uses'	=> 'NotesController@store'
));
*/
