<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BookDateRequest;
use App\Http\Controllers\Controller;

use App\Models\Catering\Package;
use App\Models\Catering\Dish;
use App\Models\Catering\Order;
use App\Models\Catering\Addon;

use App\Bookeddate;

use Session;
use Stripe\Stripe;

use Mail;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;

use App\Classes\Calendar;
use App\Classes\MgEmail;

class TestController extends Controller
{
  // public function index()
  // {
  //   $result = $this->hello();
  //
  //   echo $result;
  // }
    public function storedate(Request $request)
    {
      // don't try this in php tinker
      // it will return true but won't save in the DB
      $id = Bookeddate::Create(['datetime'=>$request->select_date])->id;

      $request->session()->put('order.date_id',$id);

      $debug = Session::get('order');

      return view('show',compact('debug','id'));
      // return redirect()->action('TestController@showdate',['id'=>$id,'debug'=>$debug]);

    }

    public function showdate(){

      return view('show');
    }

    public function mydatetimefunction(Request $request) {

        // create a date object in the following format
        $startdate = Carbon::createFromFormat('m/d/Y',$request->start);
        // echo 'Carbon::createFromFormat(m/d/Y)' . $startdate;
        // echo '</br></br>';
        $enddate = Carbon::createFromFormat('m/d/Y',$request->end);
        // echo 'Carbon::createFromFormat(m/d/Y):-- ' . $enddate;
        // echo '</br></br>';

        // returns a date formatted in the given format

        $value1 = date_format($startdate,'Y-m-d');
        $value2 = date_format($enddate,'Y-m-d');

        $f_selected_date = date("l, jS F Y",strtotime($value1));

        $f_selected_date_2 = date("l, jS F Y",strtotime($value2));

        // echo 'start date in the format (Y-m-d):--  ' . $value1 = date_format($startdate,'Y-m-d');
        // echo '</br></br>';

        // if(Bookeddate::where('datetime',$value1)->exists()){
        //   echo 'Its ALIVE !!!!';
        // }
        // else{
        //   echo '<h3>Its NOT IN HERE!!!</h3>';
        // }
        // echo '</br></br>';
        // echo 'end date in the format (Y-m-d):--  ' . $value2 = date_format($enddate,'Y-m-d');
        // echo '</br></br>';

        $input1 = $value1.'%';
        $input2 = $value2.'%';
        // echo $input1;
        // echo '</br></br>';
        // echo $input2;
        // echo '</br></br>';

        // if(Bookeddate::where('datetime',$input1)->exists())
        if($value1===$value2)
        {
          $booked = Bookeddate::where('datetime','LIKE',$input1)->get();
        }
        elseif($value1!==$value2)
        {
          $booked = Bookeddate::whereBetween('datetime',[$input1,$input2])->get();
        }
        else{
          return back()->withErrors('There has been a problem');
        }
        // $booked = Bookeddate::whereBetween('datetime',[$value1.'%',$value2.'%'])->get();
        // $booked = Bookeddate::whereBetween('datetime',['2017-02-02%','2017-02-16%'])->get();
        // debug($booked);
        // echo '<h4>Retrieve Records with time from DB based on User Input</h4>';
        // echo $booked->count() . '</br>';


        // if($booked->count()>0){
        //   echo 'booked count more than 0';
        // }
        // elseif($booked->count()===0){
        //   echo 'booked count less than 0';
        //   $arr_date_input[] = 0;
        //   debug($arr_date_input);
        // }

        // $collection = collect([1,2,3,4]);
        // $collection2 = collect([]);
        // $collection3 = 0;
        // debug($collection2);
        // debug($collection3);
        // $diff = $collection->diff($collection3);
        // $diff->all();
        // debug($diff);
        // exit;

        if($booked->count()>0){

        foreach($booked as $book){
          // echo '---';
          // echo '</br></br>';
          // echo '$book->date:- ' . $book->datetime;
          // echo '</br></br>';
          // echo '$book->time:- ' . $book->name;
          // echo '</br></br>';

          $strtotime = strtotime($book->datetime);

          $c_date = date("Y-m-d",$strtotime);

          $arr_date_input[] = $book->datetime;

          // echo 'strtotime :- ' . $strtotime = strtotime($book->datetime);
          // strtotime year month date
          // echo '</br></br>';
          // echo user input date
          // $c_date = date("l, jS , F ,Y",$strtotime);
          // echo '"l, jS , F ,Y":---   ' . $c_date = date("l, jS , F ,Y",$strtotime);
          //
          // echo '</br></br>';

          // echo '"Y-m-d":---   ' . $c_date = date("Y-m-d",$strtotime);
          //
          // echo '</br></br>';

          // debug($arr_date_input);
          }
        }
        else
        {

          $arr_date_input = 0;

        }
        // echo '<h4>---End Retrieve---</h4>';
        // debug($arr);
        // echo '<h4>Array of formatted dates based on records retrieved from DB</h4>';
        // echo '<h4>Not available for booking</h4>';
        //
        // debug($arr_date_input);
        // echo '</br></br>';



        $stripforwardslash = str_replace('/','',$request->start);
        // echo 'strip slash start date (strip the slashes form the date): ' . $stripforwardslash . '</br>';
        $stripforwardslash_end = str_replace('/','',$request->end);
        // echo 'strip slash end date (strip the slashes form the date): ' . $stripforwardslash_end . '</br>';
        // echo '</br></br>';
        $mth = substr($stripforwardslash,0,2);
        $day = substr($stripforwardslash,2,2);
        $year = substr($stripforwardslash,4,4);

        $start_format_date = date("l, jS , F ,Y",mktime(0, 0, 0, $mth, $day, $year));
        // echo '<h4>start formatted date:</h4> ' . $start_format_date;
        // echo '</br></br>';
        $end_mth = substr($stripforwardslash_end,0,2);
        $end_day = substr($stripforwardslash_end,2,2);
        $end_year = substr($stripforwardslash_end,4,4);

        $end_format_date = date("l, jS , F ,Y",mktime(0, 0, 0, $end_mth, $end_day, $end_year));
        // echo '<h4>end date formatted date:</h4> ' . $end_format_date;
        // echo '</br></br>';

        $cal_days_start_month = cal_days_in_month(CAL_GREGORIAN, $mth, $year);
        // echo 'number of days in the month of start date :' . $cal_days_start_month . '</br>';
        //
        // echo '</br></br>';

        $cal_days_end_month = cal_days_in_month(CAL_GREGORIAN, $end_mth, $end_year);
        // echo 'number of days in the month of end date :' . $cal_days_end_month . '</br>';
        // echo '</br></br>';

        $date_info_start_date = getdate(mktime(0,0,0,$mth,$day,$year));
        // debug($date_info_start_date);
        // echo $date_info_start_date['yday'];

        $date_info_end_date = getdate(mktime(0,0,0,$end_mth,$end_day,$end_year));
        // debug($date_info_end_date);
        // echo $date_info_end_date['yday'];

        // echo '</br></br>';

        $yday_start = $date_info_start_date['yday'];

        $yday_end = $date_info_end_date['yday'];

        // echo '</br></br>';



        // form
        // run through all dates
        if($yday_start<=$yday_end){
        // echo '<form>';
        while($mth<=$end_mth)
        {
        $m = $mth;

        $cal_days_current_month = cal_days_in_month(CAL_GREGORIAN, $m, $year);

        // echo 'number of days in the current month :' . $cal_days_current_month . '</br></br></br>';

        while($date_info_start_date['yday']<=$date_info_end_date['yday'])

          {
            $i = $day;

            // even when $i exceeds max num of days available in the month it will auto increment its month value
            $format_date = date("l, jS , F ,Y",mktime(0, 0, 0, $m, $i, $year));

            // we use mktime function again here to get the date in Y-m-d format
            $c_date = date("Y-m-d",mktime(0, 0, 0, $m, $i, $year));

            // echo '---';
            // echo '</br></br>';
            // echo '<input type="radio" id="time" name="time" value="' . $format_date . '">' . $date_info_start_date['yday'] . '.' . $format_date;
            // echo '</br></br>';
            // echo 'date in Y-m-d format:-> ' . $c_date;
            // echo '</br></br>';
            // echo '$date_info_start_date:[yday] ->  ' . $date_info_start_date['yday'];
            // echo '</br></br>';
            // echo 'the current $day value (increment constantly) - ' . $day;
            // echo '</br></br>';
            // echo 'last day of the current month: - '  . date("z, l, jS , F ,Y", strtotime('last day of',mktime(0,0,0,$m,$day,$year)));
            // echo '</br></br>';
            // mktime — Get Unix timestamp for a date

            $date_info_start_date['yday']++;
            $day++;
            // $arr_date_range[] = array('date' => $format_date,'time'=>'12:00:00');
            // $arr_date_range[] = array('date' => $format_date,'time'=>'18:00:00');

            $arr_date_range[] = $c_date . ' 12:00:00';
            $arr_date_range[] = $c_date . ' 18:00:00';


          }
          $mth++;

        }
        // echo '</form>';
        // end form

        }
        // echo '<h4>Array of formatted date range based on User Inputted Range</h4>';
        // echo '<h4>$arr date range</h4>';

        // we use the server for heavy lifting then move back to memory
        $collection = collect($arr_date_range);
        $collection2 = collect($arr_date_input);
        // echo 'collection count 1 : ' . $collection->count();
        //
        // echo '</br></br>';
        //
        // echo 'collection count 2 : ' . $collection2->count();

        $diff = $collection->diff($collection2);

        if($diff->count()===0){
          $diff = 0;
          // echo 'diff is empty';
          // echo '</br></br>';
          // echo $diff;
          // echo '</br></br>';
        }
        else{
          $diff = $diff;
        }

        // echo '<h4>Available date time for booking</h4>';
        // debug($diff);





        if(!empty($diff)&&count($diff)>0){

          foreach($diff as $key=>$date){
            // echo $date;
            // echo '</br></br>';
          }
        }


        // debug($diff);
        // debug($booked);
        // echo $booked->count();
        // echo $diff->count();
        //
        // dd($request->all());
        return view('show',compact('diff','arr_date_input','f_selected_date','f_selected_date_2'));



        echo '</br></br>';
        echo '---';
        echo '</br></br>';

        // foreach
        // echo form
        echo '<form>';
        echo '<h4> Radio button format: These are the dates available for booking</h4>';
        echo '<h4> Now we simply let user choose the dates and save them</h4>';
        foreach ($diff as $key => $datetime){
        echo '<input type="radio" name="booked" id="booked" value="' . $datetime . '"/>' . $datetime ;
        echo '</br></br>';
        }
        echo '</form>';

        // end form


        debug($arr_date_range);
        debug($arr_date_input);
        echo '</br></br>';
        $array1[] = array(array("a" => "green", "red"),array("a" => "green", "red"));

        $array2[] = array("a" => "blue", "red");
        $array2[1] = array("b" => "black", "pink");
        $array3[] = array("a" => "green", "red");

        $y = $yday_end - $yday_start;

        echo '---';
        debug($array1[0]);
        debug($array1);
        debug($array2);
        debug($array3);
        $array_flatten = array_flatten($array1);
        debug($array_flatten);

        $result = array_diff_assoc($array3[0], $array2[0]);
        debug($result);
      exit;

      $step = CarbonInterval::day();

      $stripforwardslash = str_replace('/','',$request->start);
      echo $stripforwardslash . '</br>';

      $day = substr($stripforwardslash,0,2);

      $mth = substr($stripforwardslash,2,2);

      $year = substr($stripforwardslash,4,4);

      $stripforwardslash_end = str_replace('/','',$request->end);
      echo $stripforwardslash . '</br>';

      $end_day = substr($stripforwardslash_end,0,2);

      $end_mth = substr($stripforwardslash_end,2,2);

      $end_year = substr($stripforwardslash_end,4,4);

      $carbon = Carbon::createFromDate($year,$mth,$day);
      echo 'carbon:createfromdate: ' . $carbon . '</br>';

      // mktime — Get Unix timestamp for a date
      $format_date = date("l, jS , F ,Y",mktime(0, 0, 0, $mth, $day, $year));
      echo $format_date . '</br>';

      $num_days_start = cal_days_in_month(CAL_GREGORIAN, $mth, $year);
      echo 'number of days in the month of start date :' . $num_days_start . '</br>';


      $getdate_start = getdate(mktime($mth,$day,$year));
      debug($getdate_start);
      echo $getdate_start['yday'];

      $getdate_end = getdate(mktime($end_mth,$end_day,$end_year));
      debug($getdate_end);
      echo $getdate_end['yday'];



      //
      // echo 'Carbon::CreateFromDate ' . '</br>';
      //
      // echo Carbon::createFromDate($year,$mth,$day,0);

      exit;

      $startdate = strtotime($request->start);
      echo date_format($request->start,'Y-m-d');

      $getdate = getdate(strtotime($startdate));

      debug($getdate);

      $dt1 = Carbon::create(2014,1,1);

      echo $dt1;
      exit;
      $bookeddate = Bookeddate::where('id',1)->first();

      debug($bookeddate);

      $nowMonth = date('F');

      echo $nowMonth . '</br>';

      $firstJanuary = getdate(strtotime('first day of January 2017'));

      debug($firstJanuary) . '</br>';

      $current = Carbon::now();

      $addthirty = $current->addDays(30);

      $start = Carbon::parse($current);

      $end = Carbon::parse($addthirty);

      $today = Carbon::today();

      echo $current . '</br>';

      echo 'today : ' . $today->toDateString() . '</br>';

      echo $today;

      $RequestAll = $request->all();

      dd($RequestAll);

    }



    public function bookdate(Request $request){

      $RequestAll = $request->all();

      dd($RequestAll);

    }


    public function index()
    {

          $mailgun_public_api_key = 'pubkey-b3ee9e626858c8169cfe2a52b28fc65b';
          $mg_email = new MgEmail($mailgun_public_api_key);
          $email = 'someemail@yhoo.com';

          if($mg_email->is_valid($email)){
            echo $email . ' is valid';
          }
          else {
            echo $email . 'is invalid' . '</br>';
            if($mg_email->spell_check){
              echo 'Do you mean ' . $mg_email->spell_check . ' ?';
            }
          }

          exit;



      // $FmyFunctions1 = new myFunctions;
      //
      // $is_ok = ($FmyFunctions1->is_ok());
      //
      // return view('test',['is_ok'=>$is_ok]);
      // $calendar = new Calendar(6,2016);
      Session::forget('order');

      $calendar = new Calendar(6,2016);

      return view('test',['calendar'=>$calendar]);

      exit;

      $a = sprintf('%0-5.2F', 1234567);
      echo $a;
      exit;

      $arr = config('package.catering_times');
      dd($arr);
      $date = 04042017;
      echo $date . '</br>';
      $getdate = (string)$date;
      echo $getdate . '</br>';
      $getday = substr($getdate,0,2);
      echo $getday;
      $getmth = substr($getdate,2,2);
      echo $getmth;
      $getyear = substr($getdate,4,4);
      echo $getyear;

      $format_date = date("l, jS , F ,Y",mktime(0, 0, 0, $getmth, $getday, $getyear));

      dd($format_date);
      // dd($getdate);
      $formatdate = strftime("%d, %B, %G, %A", mktime(0, 0, 0, $getmth, $getday, $getyear));

      dd($formatdate);



      return 'hello world';
      $package_info = Package::all()->where('id',$id)->first();
      echo'<pre>';
      print_r($package_info) . '</br>';
      echo '</pre>';

      $salad_dish = $package_info->dishes()->where('section','salad')->get();

      foreach($salad_dish as $salad)
      {
        echo $salad->name . '</br>';
      }

      $dimsum_dish = $package_info->dishes()->where('section','dimsum')->get();

      foreach($dimsum_dish as $dimsum)
      {
        echo $dimsum->name .'</br>';
      }

      dd($salad_dish);



      // $result = Order::where('id',11)->first();
      // $result_r = $result->package()->get();
      // // $result_rr = $result_r->where('id',7)->first();
      // // echo 'min pax '. $result_rr->min_pax . '</br>';
      // $result_rrr = ($result_r[0]["min_pax"]);
      // echo $result_rrr;
      // dd($result_r);



      // $addons = Addon::where('type','addons')->get();

      $addons = Addon::all()->where('type','addons');

      dd($addons);


      $tea_packs = Package::all()->where('type','hightea_main');
      $asian_packs = Package::all()->where('type','asian');
      $bento_packs = Package::all()->where('type','bento');
      $thai_packs = Package::all()->where('type','thai');

      $bento_std = $bento_packs->where('name','Bento Standard')->first();
      $bento_value = $bento_packs->where('name','Bento Value')->first();
      $tea_pack = $tea_packs->where('name','High Tea Package')->first();
      $asian_value = $asian_packs->where('name','Asian Value Package')->first();
      $asian_std = $asian_packs->where('name','Asian Standard Package')->first();
      $asian_gast = $asian_packs->where('name','Asian Gastronomy Package')->first();
      $thai_value = $thai_packs->where('name','Thai Value Package')->first();
      $thai_std = $thai_packs->where('name','Thai Standard Package')->first();
      $thai_gast = $thai_packs->where('name','Thai Gastronomy Package')->first();

      echo '1. ' . $bento_std->id . '</br>';
      echo '2. ' . $bento_value->id . '</br>';
      echo '3. ' . $tea_pack->id . '</br>';
      echo '4. ' . $asian_value->id . '</br>';
      echo '5. ' . $asian_std->id . '</br>';
      echo '6. ' . $asian_gast->id . '</br>';
      echo '7. ' . $thai_value->id . '</br>';
      echo '8. ' . $tea_pack->id . '</br>';
      echo '9. ' . $thai_std->id . '</br>';
      echo '10. ' . $thai_gast->id . '</br>';

      $asian_gast_packages = Package::where('name','Asian Gastronomy Package')->get()->first();
      $asian_id = $asian_gast_packages->id;
      echo $asian_id;

      dd($bento_packs);

      $result = Order::where('id',1)->first();
      $result_r = $result->package()->get();
      echo $result_r->name;
      // returns collection of 'High Tea Package' with 'id'=1. This is correct because the 'High Tea Package' form is displayed when I submitted the form. This relationship allows me to get the id of the package with just the order id even when the name of the package is not saved in orders table
      // public function package() {
      //   return $this->belongsTo(Package::class,'id');// 'foreign_key', 'local_key');
      // }
      // public function orders(){
      //   return $this->hasMany(Order::class,'id');
      // }
      //example->Package::class,'package_id','order_id'
      // $result_r = $result->package['id'];

      dd($result_r);

      $asian_meals = array();
      $asian_value_packages = Package::where('name','Asian Value Package')->get()->first();
      array_push ($asian_meals,
      [$asian_value_packages->dishes()->where('section','vegetable')->get()],
      [$asian_value_packages->dishes()->where('section','main')->get()]
      // $asian_vegetable = $asian_value_packages->dishes()->where('section','vegetable')->get();
      // $asian_fish = $asian_value_packages->dishes()->where('section','fish')->get();
      // $asian_chicken = $asian_value_packages->dishes()->where('section','chicken')->get();
      // $asian_appetizer = $asian_value_packages->dishes()->where('section','appetizer')->get();
      // $asian_dessert = $asian_value_packages->dishes()->where('section','dessert')->get();
      // $asian_beverage = $asian_value_packages->dishes()->where('section','beverage')->get();
      );

      echo '</br></br>';

      echo '-------';

      foreach($asian_meals as $key => $value)
      {
        $mykey = $key;
        $myvalue = $value;
        echo '$mykey = ' . $mykey . '</br>';

      }

      echo '</br></br>';

      $asian_standard_packages = Package::where('name','=','Asian Standard Package')->get()->first();

      echo '$asian_standard_package name -> ' . $asian_standard_packages->name . '</br>';

      $asian_standard_dishes = $asian_standard_packages->dishes()->where('section','main')->get();

      echo '$asian_standard_dishes -> ' . $asian_standard_dishes;
      echo '</br></br>';
      echo 'var_dump-> ';
      var_dump($asian_meals[0][0][0]["name"]);



      // dd($asian_meals);
//       array:2 [▼
//   0 => array:1 [▼
//     0 => Collection {#212 ▼
//       #items: array:5 [▼
//         0 => Dish {#218 ▼
//           #table: "dishes"
//           #hidden: array:2 [▶]
//           #dates: []
//           #fillable: array:2 [▶]
//           #guarded: []
//           #connection: null
//           #primaryKey: "id"
//           #perPage: 15
//           +incrementing: true
//           +timestamps: true
//           #attributes: array:5 [▼
//             "id" => 180
//             "name" => "Mixed Vegetables with Cabbage"
//             "section" => "vegetable"
//             "created_at" => "2017-01-19 02:36:24"
//             "updated_at" => "2017-01-19 02:36:24"
//           ]
//           #original: array:7 [▶]
//           #relations: array:1 [▶]
//           #visible: []
//           #appends: []
//           #dateFormat: null
//           #casts: []
//           #touches: []
//           #observables: []
//           #with: []
//           #morphClass: null
//           +exists: true
//           +wasRecentlyCreated: false
//         }
//         1 => Dish {#219 ▶}
//         2 => Dish {#220 ▶}
//         3 => Dish {#221 ▶}
//         4 => Dish {#222 ▶}
//       ]
//     }
//   ]
//   1 => array:1 [▼
//     0 => Collection {#223 ▼
//       #items: array:6 [▶]
//     }
//   ]
// ]

      // dd($asian_meals[0][0][0]["name"]);
      // "Mixed Vegetables with Cabbage"

      echo '</br></br>';

      $asian_gast_packages = Package::where('name','=','Asian Gastronomy Package')->get()->first();

      echo '$asian_gastronomy_packages-> ' . $asian_gast_packages->name . '</br>';

      $asian_gast_dishes = $asian_gast_packages->dishes()->where('section','main')->get();

      foreach($asian_gast_dishes as $asian_dish){
        echo $asian_dish->name . '</br>';
      }


      // $asian_standard_packages = Package::where('name','=','Asian Standard Package')->get()->first();
      //
      // echo $asian_standard_packages->name . '</br>';
      //
      // $asian_standard_dishes = $asian_standard_packages->dishes()->where('section','main')->get();
      //
      // foreach($asian_standard_dishes as $asian_dish){
      //   echo $asian_dish->name . '</br>';
      // }
      // unset($asian_value);


      // $asian_standard_packages = Package::where('name','Asian Value Package')->get()->first();
      //
      // $asian_standard_dishes = $asian_standard_packages->dishes()->where('section','main');
      //
      // $asian_gastr_packages = Package::where('name','Asian Value Package')->get()->first();
      //
      // $asian_gastr_dishes = $asian_gastr_packages->dishes()->where('section','main');

      // foreach($asian_value_dishes as $value_dish){
      //   echo $value_dish->name . '</br>';
      // }
      // unset($value_dish);

      echo '</br></br>';

      $package = Package::where('name','=','High Tea Package (3 Dishes)')->get()->first();

      echo 'id -> ' . $package->id . '</br>';

      echo '1 ->' . $package->name . '</br>';

      echo '2 ->' . $package->price_pax . '</br>';

      echo 'Maximum number of dishes this pack is allowed to have -> ' . $package->limit . '</br></br>';


      $json = $package->rules;

      var_dump($json);

      echo '</br></br>';

      $array = array(
        'salad' => 1,
        'sandwich' =>1,
        'chicken' =>1,
        'dim sum' => 1,
        'kueh' => 0,
        'finger food' => 0,
        'pasteries' => 0,
        'dessert' => 1,
        'beverage' => 1,
      );

      $array_sum = array_sum($array);

      echo 'Array Sum => ' . $array_sum;

      echo '</br></br>';

      foreach($json as $key=>$value){
        if($value = 1) {
          echo 'this ' . $key . ' has a value equals to 1</br></br>';
        }
      }

      $array_diff = array_diff($array,$json);

      print_r($array_diff);

      // return response()->json([
      //   'name' => 'Abigail',
      //   'state' => 'CA'
      // ]);


      // dd($kueh_dishes->toArray());
      //
      // var_dump($dimsum_dishes);
      //
      // var_dump($all_dessert);
      //
      // dd($all_dessert->toArray());

      // $section_name = $package->dishes()->get();
      //
      // foreach ($section_name as $sec) {
      //   echo $sec->section . '</br>';
      // }
      //
      //
      // $dish_name = $package->dishes()->where('section','Salad')->get();
      //
      //
      // foreach ($dish_name as $dish){
      //   echo $dish->name . '</br>';
      // }
      //
      // var_dump($package->rules);








      /*if(get_magic_quotes_gpc()){
          $d = stripslashes($arr);
          }else{
              $d = $arr;
              }
              $d = json_decode($d,true);

              var_dump($d);*/

      //$json = '[{"limit":3,"salad":1,"sandwich":1,"chicken":1,"dim_sum":1,"kueh":1,"finger_food":1,"pasteries":1,"dessert":1,"beverage":1}]';









      //$package = Package::find(13);

      /*$package1 = Package::where('name','=','Asian Gastronomy Package')->get()->first();

      $package2 = Package::where('name','=','Asian Gastronomy Package')->get();

      $dish = Dish::where('name','=','Pandan Leaf Chicken')->get()->first();

      $dish2 = Dish::where('name','=','Thai Basil Chicken')->get();

      dd($dish2->packages);*/

      /*
      foreach ($sectiondishes as $dish){
        echo $dish->name . '</br>';
      }
      */

      /*
      $high_tea_all = Package::where('name','High Tea Package (3 Dishes)')->get()->first();

      $high_tea_salad = $high_tea_all->dishes()->where('section','Salad')->get();

      return $high_tea_salad->toArray();

      $main_dishes = Dish::where('section','Main')->get();

      $number = $main_dishes->count();
      echo 'Number of results for main dishes:' . $number . '</br></br>';

      $x = 1;

      echo'<strong>All Main Dishes in all packages</strong></br>';

      foreach ($main_dishes as $main_dish)

        {
          echo $x . '.  ' . $main_dish->name . '</br>';
          $x++;
        }


      echo '</br>';



      $chicken_dishes = Dish::where('section','Chicken')->get();

      echo'<strong>All Chicken Dishes in all packages</strong></br>';

      foreach ($chicken_dishes as $chicken_dish){
        echo $chicken_dish->name . '</br>';
      }


      echo'</br>';


      $package = Package::where('name','=','Asian Gastronomy Package')->get()->first();
      echo'<b>All dishes in Asian Gastronomy</b></br>';

      $package_number = $package->count();

      echo 'Total number of packages available = ' . $package_number . '</br>';

      echo 'Number of dishes in gastronomy = ' . $package->dishes->count() . '</br></br>';

      foreach($package->dishes as $dish){

        echo $dish->name . '</br>';

      }
      echo'</br>';

      $desserts = $package->dishes()->where('section','=','Dessert')->get();
      echo '<b>All Desserts in Asian Gastronomy</b></br>';
      foreach($desserts as $dessert){

        echo $dessert->name. '</br>';

      }

      echo'</br>';

      */



      // debug($package1);
      //
      // debug($package2);

      // foreach($package->dishes as $dish){
      //
      //   echo $dish->name . '</br>';
      //
      // }


    }
}
