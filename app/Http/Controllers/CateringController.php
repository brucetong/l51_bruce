<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\DeliveryInfoRequest;
use App\Http\Requests\AddonDishRequest;
use App\Http\Controllers\Controller;

use App\Models\Catering\Package;
use App\Models\Catering\Dish;
use App\Models\Catering\Order;
use App\Models\Catering\Addon;
use App\Models\Catering\StripeCheckout;

use Session;

use Mail;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;

use App\Classes\MgEmail;

class CateringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //start page
     //do not change

    public function index()
    {

      // $tea_packs = Package::all()->where('type','hightea_main');
      $asian_packs = Package::all()->where('type','asian');
      $bento_packs = Package::all()->where('type','bento');
      $thai_packs = Package::all()->where('type','thai');

      $bento_std = $bento_packs->where('name','Bento Standard')->first();
      $bento_value = $bento_packs->where('name','Bento Value')->first();
      // $tea_pack = $tea_packs->where('name','High Tea Package')->first();
      $asian_value = $asian_packs->where('name','Asian Value Package')->first();
      $asian_std = $asian_packs->where('name','Asian Standard Package')->first();
      $asian_gast = $asian_packs->where('name','Asian Gastronomy Package')->first();

      $thai_value = $thai_packs->where('name','Thai Value Package')->first();
      $thai_std = $thai_packs->where('name','Thai Standard Package')->first();
      $thai_gast = $thai_packs->where('name','Thai Gastronomy Package')->first();


      return view('catering.package.category.index',compact('bento_std','bento_value','tea_pack','asian_value','asian_std','asian_gast','thai_value','thai_std','thai_gast'));

      // return View::make('catering.package.category.index');
    }

    public function postSavePackage(Request $request) {

    $this->validate($request, [

    ]);
    $selection = Order::create(
        // 'select_package'=>$request->input('selection'),
        // 'salad'=>$request->input('salad'),
        // 'sandwich'=>$request->input('sandwich')]
    );
    // $selection->save();

    // $request->session()->put('selection',$selection);

    $postSavePackage = $request->all();
    //print_r($inputSavePackage);
    $request->session()->put('order.dishes',$postSavePackage);//store data into session
    $allOrder = $request->session()->get('order');//accessing the session

    return redirect()->action('CateringController@getOrderCheckOut1');
    //return response()->json($allOrder);//returns json

    // Array
    // (
    //   [_token] => c7lePuGbekNXAPA0kz7QHkjj2G89Evt6HDKW9Bqd
    //   [selection] => High Tea Package 3 Dishes Min. 100 pax
    //   )
    $response = array ('status' => 'success',
                        'msg' => 'You chose ' . $selection['select_package'] . ' with ' . $selection['salad'] . '. Package saved successfully');
    // return response()->json ($response,200,[],JSON_PRETTY_PRINT);
    // 'msg' => 'You chose ' . $request->input('selection') . '. Package saved successfully',);
    // print_r($selection);
    return response()->json($response);
    // return response()->json ($request);
    // {"attributes":{},"request":{},"query":{},"server":{},"files":{},"cookies":{},"headers":{}}

  }//postSavePackage


  //save the form data from add on side dishes
  public function postCheckOutAddon(AddonDishRequest $request){

    // $request->session()->flush();

    settype($total,"int");
    settype($total_per_dish,"double");

    //get the form data
    // $selected_addons = $request->all();
    //
    // dd($selected_addons);
    //count how many dishes there are in add on dishes
    $addon_dish_num = Addon::count();

    //loop through and see if form field is populated and put them into addon key within session()
    $addOns = Addon::where('type','addons')->get();
    $i=0;
    while($i<$addon_dish_num)
    {
          $arr[] =  ($addOns[$i]['slug']);
          if(isset($request->$arr[$i])&&$request->$arr[$i]!=null)
          {
          $Addon=Addon::all()->where('slug',$arr[$i])->first();
          // $request->session()->put('order.addon', $request->$arr[$i]);
          // echo 'this will only appear if not null:  ' . $arr[$i] . ' = ' . $request->$arr[$i] . '</br>';
          //store as an assoc array first

          $addon_dishes[$Addon->name] = $request->$arr[$i];

          // debug($arr[$i]);
          // fetch price per addons frm DB
          // $addon_id = $request->$arr[$i];
          // debug($addon_id);

          // debug($Addon);
          $fetchprice=$Addon->price_pax;
          // echo $fetchprice;

          $total_per_dish = $fetchprice * $request->$arr[$i];
          // echo 'total per dish: $  ' . $total_per_dish;
          // exit;
          $total += $total_per_dish;

          $addon_dish_total[$Addon->name] = $total_per_dish;
          //put the assoc array into session 'order' with 'addons' key
          $request->session()->put('order.addons', $addon_dishes);//put array into session with addons key
          $request->session()->put('order.addons_price',$addon_dish_total);
          $request->session()->put('order.addon_total_price',$total);

          // $allOrder = $request->session()->get('order');//accessing the session
          // debug($fetch_price);
          // dd($allOrder);
          }
          $i++;
    }//end loop

    // minimum_pax is the pax used in calculating package price
    // minimum_pax is the greater of the two pax sizes
    // (!empty($order['addon_total_price'])?$order['addon_total_price']:0);

      //redirect to where we collect user info
    return redirect()->action('CateringController@getUserInfo');

    }

    //save user and delivery info
    public function postUserInfo(DeliveryInfoRequest $request)
    {
          // email validation with Mailgun
          $mailgun_public_api_key = 'pubkey-b3ee9e626858c8169cfe2a52b28fc65b';
          $mg_email = new MgEmail($mailgun_public_api_key);
          $email = $request->cust_email;

          if(!$mg_email->is_valid($email)){
            return back()->withErrors('You did not enter a valid email address');
          }
          else {


          // delivery surcharge
          $strip_code = mb_substr($request->postal_code,0,2);

          // $postalcodes =array(1,2,3,4,5,6,7,8,9,62,63);
          $postalcodes = config('package.postalcodes');
          $a = (int)$strip_code;

          $array_flip = array_flip($postalcodes);

          if(array_key_exists($a,$array_flip))
          {
          $delivery_surcharge=10;
          $request->session()->put('order.cbd_surchage',$delivery_surcharge);
          }

          $deliveryinfo = $request->all();

          foreach ($deliveryinfo as $key => $value){
            if(strpos($key,'_token')===FALSE)
            {
              $arr[$key] = $value;
            }
          }

          $i = $request->catering_time;

          $config_times = config('package.catering_times');

          $get_time = $config_times[$i];
          // convert date format
          $getdate = $request->catering_date;

          $get_day = substr($getdate,0,2);

          $get_mth = substr($getdate,2,2);

          $get_year = substr($getdate,4,4);

          $format_date = date("l, jS , F ,Y",mktime(0, 0, 0, $get_mth, $get_day, $get_year));

          $arr['formatted_date'] = $format_date;
          $arr['formatted_time'] = $get_time;
          $request->session()->put('order.delivery_info',$arr);
          // $userDetails = $request->all();
          //
          // $request->session()->put('order.userinfo',$userDetails);
          $public_holidays = config('package.public_holidays');

          $entered_date = $request->catering_date;

          $array_holiday_flip = array_flip($public_holidays);

          if(array_key_exists($entered_date,$array_holiday_flip)){
            // additional one dollar per pax on holiday
            // please take note
            $per_pax_charge_holiday = 1;
            $total_charge_holiday = 1 * $request->session()->get('order.minimum_pax');
            $request->session()->put('order.public_holiday_surcharge',$total_charge_holiday);
          }
          // return view('catering.package.category.address',compact('debug'));
          return redirect()->action('CateringController@getCart');//redirect to action with debug variable. note the dollar sign and lack of brackets
          // return redirect()->action('CateringController@getCart');

          // flush the sesson data
          // $request->session()->flush();

          exit;
        }
          // return response()->json($allOrder,200,[],JSON_PRETTY_PRINT);

    }
    // after choosing add on dishes we go to the cart page
    // we confirm with customer before paying
    //  cart
    public function getCart(Request $request) {

      // if( !Session::has('order.selected_dishes') && !Session::has('order.addons')){
      //
      // }

      $debug = $request->session()->get('order');

      $order = $request->session()->get('order');

      $package_price = (!empty($order['package_price'])?$order['package_price']:0);
      // add on dishes if it is not empty
      $addon_dishes_price = (!empty($order['addon_total_price'])?$order['addon_total_price']:0);
      // add surcharge from drinks and desserts
      $drinks_and_dessert_surcharge = (!empty($order['drinks_dessert_total_surcharge'])?$order['drinks_dessert_total_surcharge']:0);
      // add surcharge for holiday events
      $holiday_surcharge = (!empty($order['public_holiday_surcharge'])?$order['public_holiday_surcharge']:0);

      $package_plus_drinks_dessert = $drinks_and_dessert_surcharge + $package_price;
      // holiday + add on + package + drinks dessert
      $sum = $package_plus_drinks_dessert + $addon_dishes_price + $holiday_surcharge;
      // if total order below $500 we must add $35 additional charge
      $min_order_extra_charge = (($sum<500)?35:0);

      $request->session()->put('order.below_five_hundered_surcharge');

      $grand_total = $sum + $min_order_extra_charge;

      $calculate_gst = 0.07 * $grand_total;

      $grand_total_gst = round($calculate_gst + $grand_total, 2);

      $request->session()->put('order.grand_total_gst',$grand_total_gst);
      // return to view

      return view('catering.package.category.cart',compact( 'debug','order','package_plus_drinks_dessert','calculate_gst','grand_total_gst','grand_total' ));

    }
    //checkout
    public function getCheckout(Request $request) {

      $total = $request->session()->get('order.grand_total_gst');

      return view('catering.package.category.checkout', compact('total'));
    }

    public function postCheckout(Request $request){
      // dd($request->all());
      $total = $request->session()->get('order.grand_total_gst');

      Stripe::setApiKey('sk_test_61ri35737wsM6fkpBYipGyV6');

        try{

          $charge = Charge::create(array(
            "amount" => $total * 100,
            "currency" => "sgd",
            "source" => $request->Input('stripeToken'), // obtained with Stripe.js
            "receipt_email" => $request->email,
            // "email" => $request->Input('stripeEmail'),
            "description" => "Test Charge"
          ));

          }
        catch (\Exception $e){

          return redirect()->route('checkout-error')->with('error', $e->getMessage());
          }
        // $stripecheckout = new StripeCheckout;
        // $stripecheckout->charge_id = $chargeID->id;
        // $stripecheckout->stripe_amount = $chargeID->amount/100;
        //
        // $stripecheckout->outcome = $chargeID->outcome;
        // $stripecheckout->save();
          echo 'success' . '</br>';
          dd($charge);
          Session::forget('order');
          return redirect()->route('checkout')->with('success','Successfully purchased catering package. Thank you for doing business with us!');
    }

    public function getUserInfo(Request $request) {

      if(!Session::has('order.selected_dishes')){
        return redirect('index')->with('error', 'Select dishes first!');
        exit;
      }

        $debug = $request->session()->get('order');
        return view('catering.package.category.address',compact('debug'));

    }

    // Add On Dishes
    public function getCheckOutAddon(Request $request) {

      if(!Session::has('order.selected_dishes')){
        return redirect('index')->with('error', 'Select dishes first!');
        exit;
      }

      $get_dessert_surcharge = (Session::has('order.dessert_surcharge_per_pax')?Session::get('order.dessert_surcharge_per_pax'):0);
      $get_drinks_surcharge = (Session::has('order.drinks_surcharge_per_pax')?Session::get('order.drinks_surcharge_per_pax'):0);

      $total_drinks_dessert_surcharges = ($get_dessert_surcharge + $get_drinks_surcharge) *  Session::get('order.minimum_pax');

      $request->session()->put('order.drinks_dessert_total_surcharge',$total_drinks_dessert_surcharges);

      $debug = $request->session()->get('order');
      // we use this to populate the add on dishes menu
      $addons = Addon::all()->where('type','addons');
      return view('catering.package.category.addon', compact('addons','debug'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function flushData(Request $request){
      $request->session()->flush();
      $request->session()->forget('order');
      return back();
    }

    public function flush()
    {
      return view('catering.package.category.flush');
    }

    public function getCheckoutError() {
      return view('catering.package.category.checkout-error');
    }

}
