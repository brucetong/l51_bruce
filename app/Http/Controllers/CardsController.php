<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Card;
use App\Models\Note;

class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$card = DB::table('card')->get();

        $cards = Card::all();
        return view('card.index', compact('cards'));
    }


    //We provide a list of cards to delete
    public function deletecards()
    {
      $delete_cards = Card::all();

      return view('card.delete', compact('delete_cards'));
      //we return an array of cards to delete with this key
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('card.create');
    }
    //update our records
    public function updatelist()
    {
        $update = Card::all();

        return view('card.update', compact('update'));
    }

    //we use the function 'destroy' to delete records
    public function destroy( Request $request)
    {
        $id=$request->id;

        //print_r($id);

        Card::destroy($id);

        /*DB::table('cards')->where('id','=',$id)->delete();*/

        return 'Deleted';

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $title=$request->title;
       $card=new Card;
       $card->title=$title;
       $card->save();

       return 'Saved';

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $card = Card::find($id);

        return view('card.show',compact('card'));

        //return $card;
    }


    public function update(Request $request)
    {

      //$title=$request->newvalue;
      //print_r $title;

      $card=Card::find($request->card_id);

      if($card)
      {
        //$card->update(['title'=>$request->new]);
        $card->title = $request->title;
        $card->save();
      }

      return redirect()->route('update-list');

      //return 'Updated';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
    *public function update(Request $request, $id)
    *{}
    **/


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
