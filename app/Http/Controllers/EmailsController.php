<?php

namespace App\Http\Controllers;

use Mail;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailsController extends Controller
{
    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function html_email(Request $request)
    {
        $name = $request->name;
        $body = $request->body;

        $data = array('name' => "$name",
                      'body' => "$body");

        Mail::send('email.mail', $data, function($message) {

          $message->to('tongcheong77@gmail.com', 'Laravel Email Tests');

          $message->subject('Sending email with Mailgun and Laravel is awesome!');

          $message->from('bompipi@app.com', 'Bompipi');

        });

        echo "HTML email sent. Please check your inbox";

    }


public function show_html_form() {
  return view('email.sendhtml');
}
}
