<?php

namespace App\Http\Controllers;

use App\Page;
//use Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Requests\CreatePage;
use App\Http\Controllers\Controller;





class PagesController extends Controller
{

    public function about(){

      $second = 'Mr Bruce Tong';

      //return view('templates.catering.about')->with('name',$name );

      /* return view('templates.catering.about')->with([
        'first' =>  'Mr. Bruce',
        'second'  =>  'Tong'
      ]); */

      /*$data = [];
      $data['first']  = 'Mr. Bruce';
      $data['second'] = ' , the most powerful man';

      return view('templates.catering.about',$data);*/

      $first  = 'Fox';

      return view('templates.catering.about',compact('first','second'));

    }

    public function store(CreatePage $request) {

      //Page::create(Request::all);

      //$input  = Request::all();

      //Page::create($input);

      //$input['published_at'] = Carbon::now();

      //use create method to insert a new record into the database

      Page::create($request->all());

      return redirect('high_tea/create')->with('status', 'Thank you. Your order has been placed. We will be in touch shortly.');

      //$input['published_at'] = Carbon::now();

      //Page::create($input);

      //return $input;

    }

    public function create(){

      return view('templates.high_tea');

    }

    public function showProfile($id){
      //$value = $request->session()->get('key');
    }

    public function index() {

      $pages = Page::all();

      return view('templates.index', compact('pages'));

      //$pages = Page::latest()->get();

      //$pages = Page::latest('published')->published()->get();

      //$pages = Page::latest('published_at')->where('published_at', '<=', Carbon::now())->get();

      //$pages = Page::order_by('published_at', 'desc')->get();

      //return view('pages.index', compact('pages'));

    }

    public function show($id){

      $page = Page::findorFail($id);

      return view('templates.show', compact('page'));

      //return json_encode($page);


      //$page = Page::findorFail($id);
      //return view('pages.show', compact('page'));
      //dd($article->created_at->addDays(8)->diffForHumans());
      //dd($article->created_at->addDays(8)->format('Y-m'));
      //dd($article->created_at->year);
      //dd($article->published_at);

    }
      /**  public function store(Request $request) {
      $this->validate($request, ['title'=>'required', 'body'=>'required']);
      Page::create($request->all());
      return redirect->('pages');
      } **/

    /** Controller imports trait ValidateRequests **/
    /** ValidateRequests uses validate(Request $request, array $rules) method **/

    public function edit($id){
      $pages = Page::findorFail($id);
      return view('templates.update');
    }
}
