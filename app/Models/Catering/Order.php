<?php

namespace App\Models\Catering;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = "orders";
  protected $hidden = ["created_at","updated_at"];
  protected $dates = [];
  protected $fillable = ["select_package","salad","sandwich","addons","min_pax","total_amount","paid"];
  protected $guarded = [];

  // protected $fillable = ["user_id","package_id","selection","pax","venue","delivery_amount","total_amount","delivery_date","status","paid"];

  public function user() {
    return $this->belongsTo(User::class);
  }
  public function package() {
    return $this->belongsTo(Package::class,'id');// 'foreign_key', 'local_key');
  }
  // public function selections() {
  //   return $this->hasMany(App\Models\Catering\Selection::class, 'order_id', 'id');
  // }
}
