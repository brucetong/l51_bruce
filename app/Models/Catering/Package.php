<?php

namespace App\Models\Catering;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
  protected $table = "packages";
  protected $hidden = ["created_at","updated_at"];
  protected $dates = [];
  protected $fillable = ["name","price_pax","min_pax","type","description","limit","rules"];
  protected $casts = ['rules' => 'array'];
  protected $guarded = [];

  /*
  public function dishes() {
    return $this->hasMany(App\Models\Catering\Dish::class, 'package_id','id');
  }
  */

  public function dishes()
  {
    return $this->belongsToMany(Dish::class,'dish_package');
  }

  public function orders(){
    return $this->hasMany(Order::class,'id');
  }
}
