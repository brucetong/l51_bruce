<?php

namespace App\Models\Catering;

use Illuminate\Database\Eloquent\Model;

class Selection extends Model
{
  protected $table = "selections";
  protected $hidden = [];
  protected $dates = ["created_at","updated_at"];
  protected $fillable = ["order_id","dish_id"];
  protected $guarded = [];

  public function order() {
    return $this->belongsTo(App\Models\Catering\Order::class);
  }

  public function dish() {
    return $this->belongsTo(App\Models\Catering\Dish::class);
  }
}
