<?php

namespace App\Models\Catering;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
  protected $table = "dishes";
  protected $hidden = ["created_at","updated_at"];
  protected $dates = [];
  protected $fillable = ["name","section"];
  protected $guarded = [];

  /*
  public function package() {
    return $this->belongsTo(App\Models\Catering\Package::class);
  }
  */

  public function packages()
  {
    return $this->belongsToMany(Package::class,'dish_package');
  }

  public function selections() {
    return $this->hasMany(Selection::class, 'dish_id', 'id');
  }

}
